USE [Camunda]
GO

/****** Object:  Table [dbo].[AXI_APPID_PIN_SESSION_MAP]    Script Date: 9/07/2015 2:57:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AXI_APPID_PIN_SESSION_MAP](
	[applicationId] [nvarchar](64) NOT NULL,
	[pin] [nvarchar](64) NOT NULL,
	[sessionToken] [nvarchar](64) NULL,
	[sessionTokenCreateTime] [timestamp] NULL
) ON [PRIMARY]

GO

