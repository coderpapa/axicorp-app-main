USE [Camunda]
GO

/****** Object:  Table [dbo].[AXI_APP_FORM_VARS_DTL]    Script Date: 9/07/2015 2:56:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AXI_APP_FORM_VARS_DTL](
	[id] [nvarchar](64) NULL,
	[formKey] [nvarchar](1000) NULL,
	[variableName] [nchar](1000) NULL,
	[variableType] [nvarchar](50) NULL,
	[filterPattern] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

