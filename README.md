# AxiCorp Online Application Form #

## Project Elements ##
* Jackson / Jax-RS based REST API classes
* Singleton Service Classes for serving the API 
* Singleton BPMN Service Classes interacting with Camunda Engine via CAM API
* JDBC Helper Classes for converting web objects to db objects
* Spring JDBC Templates for persisting to Custom DB
* Spring application config for Camunda Beans, DB and Custom Bean
* SQL Scripts for custom tables

## Pre-Req ## 
* Camunda + Tomcat
* Maven 
* Database + Application SQLs run


Assuming you have an instance of Camunda / Tomcat installed and you have cloned this project ...do the following:

## Build / Deploy On a Windows Machine ##
* Fix BuildRelease.bat to set the value for generated WAR location (Change the path "C:\Users\Alok.Mishra\workspace\ApplicationAPI\target\ApplicationAPI-0.0.1.war" to your local machine path)
* Fix BuildRelease.bat to set the value for Tomcat WebApps location (Change the path "C:\Users\Alok.Mishra\ApplyOnlineCode\Camunda\server\apache-tomcat-7.0.62\webapps" to your local machine path)
* Run BuildRelease.bat

## Build / Deploy On Non Windows Machines ##
* mvn clean
* mvn install
* cp target/ApplicationAPI-0.0.1.war ${CamundaHome}/server/{tomcat}/webapps