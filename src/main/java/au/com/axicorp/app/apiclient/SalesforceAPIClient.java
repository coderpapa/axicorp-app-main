package au.com.axicorp.app.apiclient;

import java.io.IOException;
import java.util.HashMap;

import org.camunda.bpm.engine.impl.util.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.axicorp.app.types.SFClientInfo;
import au.com.axicorp.app.types.SalesforceContactQueryRequest;
import au.com.axicorp.app.types.SalesforceContactQueryResponse;
import au.com.axicorp.app.types.SalesforceContactRequest;
import au.com.axicorp.app.types.SalesforceLeadRequest;
import au.com.axicorp.app.types.SalesforceStatRequest;
import au.com.axicorp.utils.ConfigPropertyService;

public class SalesforceAPIClient implements SalesforceAPIClientIfc{
	final static String SF_API_ROOT = ConfigPropertyService.instance().getProperties().getProperty("salesforce-api");
	
	/**
	 * Salesforce Search
	 * Request: 
	 * Response: 
	 * 
	 * @param query
	 */
	public  SalesforceContactQueryResponse search(SalesforceContactQueryRequest query){
		JSONObject data = new JSONObject(query);		
		RestTemplate restTemplate = new RestTemplate();				
		HttpHeaders headers = new HttpHeaders();		 	
	    headers.setContentType(MediaType.APPLICATION_JSON);			
		HttpEntity<String> entity = new HttpEntity<String>(data.toString(), headers);		
		ResponseEntity<String> result = restTemplate.exchange(SF_API_ROOT+"/search", HttpMethod.POST, entity, String.class);		
		SalesforceContactQueryResponse response = new SalesforceContactQueryResponse(result.getBody());				 			
		return response;
	}
	
	/**
	 * Create Lead
	 * Request: 
	 * Response: 
	 * 
	 * @param lead
	 */
	public  void createLead(SalesforceLeadRequest lead){
		
	}
	
	/**
	 * Create Contact 
	 * Request: 
	 * Response: 
	 * 
	 * @param contact
	 */
	public  void createContact(SalesforceContactRequest contact){
		
	}
	
	/**
	 * Create Form Statistics Entry
	 * Request: SalesforceStatRequest
	 * Response: SalesforceStatRequest with id of the entry
	 * 
	 * @param formStat
	 */
	public  void createFormStat(SalesforceStatRequest formStat){
		
	}
	
	/**
	 * Update Form Statistics Entry
	 * Request: id of the entry, SalesforceStatRequest as an update
	 * Response: SalesforceStatRequest with id of the entry
	 * 
	 * @param statid
	 * @param formStat
	 */
	public  void updateFormStat(String statid,SalesforceStatRequest formStat){
		
	}
	
	
	
	private static void testSearch(){
		SalesforceAPIClient c = new SalesforceAPIClient();
		SalesforceContactQueryRequest query=new SalesforceContactQueryRequest();
		query.setEmail("jack@something.com");
		c.search(query);
	}
	
	
	public static void main(String [] args){
		testSearch();
	
	}
}
