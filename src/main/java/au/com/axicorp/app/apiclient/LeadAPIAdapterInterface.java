package au.com.axicorp.app.apiclient;

import au.com.axicorp.app.types.SalesforceContactQueryRequest;
import au.com.axicorp.app.types.SalesforceContactQueryResponse;
import au.com.axicorp.app.types.SalesforceContactRequest;
import au.com.axicorp.app.types.SalesforceLeadRequest;
import au.com.axicorp.app.types.SalesforceStatRequest;

public interface LeadAPIAdapterInterface {
	/**
	 * Salesforce Search
	 * 
	 * @param query
	 */
	public  SalesforceContactQueryResponse search(SalesforceContactQueryRequest query);

	/**
	 * Create Lead
	 * 
	 * @param lead
	 */
	public  void createLead(SalesforceLeadRequest lead);

	/**
	 * Create Contact Request:
	 * 
	 * @param contact
	 */
	public  void createContact(SalesforceContactRequest contact);

	/**
	 * Create Form Statistics Entry
	 * 
	 * Request: SalesforceStatRequest 
	 * 
	 * Response: SalesforceStatRequest with id of the entry
	 * 
	 * @param formStat
	 */
	public  void createFormStat(SalesforceStatRequest formStat);

	/**
	 * Update Form Statistics Entry Request: id of the entry,
	 * SalesforceStatRequest as an update Response: SalesforceStatRequest with
	 * id of the entry
	 * 
	 * @param statid
	 * @param formStat
	 */
	public  void updateFormStat(String statid, SalesforceStatRequest formStat);

}
