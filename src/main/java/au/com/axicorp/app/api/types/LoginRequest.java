package au.com.axicorp.app.api.types;

public class LoginRequest {
	private String pin;

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}
	
}
