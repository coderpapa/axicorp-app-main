package au.com.axicorp.app.api.types;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

 
import pl.zientarski.SchemaMapper;
 
public class FormData {
	private Data[] data= new Data[]{};
	private PageInfo pageInfo = new PageInfo();
	private TrackingInfo trackingInfo = new TrackingInfo();
	private AdditionalInfo additionalInfo = new AdditionalInfo();
	
	public FormData(){
		super();
	}
	
	
	
	 



	public Data[] getData() {
		return data;
	}







	public void setData(Data[] data) {
		this.data = data;
	}







	public PageInfo getPageInfo() {
		return pageInfo;
	}



	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}



	public TrackingInfo getTrackingInfo() {
		return trackingInfo;
	}



	public void setTrackingInfo(TrackingInfo trackingInfo) {
		this.trackingInfo = trackingInfo;
	}



	public AdditionalInfo getAdditionalInfo() {
		return additionalInfo;
	}



	public void setAdditionalInfo(AdditionalInfo additionalInfo) {
		this.additionalInfo = additionalInfo;
	}


}
