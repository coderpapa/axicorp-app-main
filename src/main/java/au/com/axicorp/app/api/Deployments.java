package au.com.axicorp.app.api;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.repository.ProcessDefinition;

import au.com.axicorp.bpmn.services.BPMNProcessDeployerService;
 

@Path("deployments")
public class Deployments {

	

	/**
	 * Query Applications
	 * 
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getApplications() {
		return BPMNProcessDeployerService.instance().getDeployedProcesses();
	}

	/**
	 * Create New Deployment from property file
	 * 
	 * @param requestMap
	 * @return
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes("application/json")
	public List<String> createDeployment(Map<String, String> requestMap) {	 
		BPMNProcessDeployerService.instance().deployProcess();
		return BPMNProcessDeployerService.instance().getDeployedProcesses();
	}
	
	/**
	 * Query Applications
	 * 
	 * @return
	 */
	@GET
	@Path("/processes")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ProcessDefinition> getProcessDefinitions() {
		return BPMNProcessDeployerService.instance().getProcessDefinitions();
	}

}
