package au.com.axicorp.app.api.types;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

import au.com.axicorp.bpmn.services.BPMNEngineProxyService;
import au.com.axicorp.utils.Utils;

public class AxiApplicationTask {
	
	private String taskNumber;
	private String taskFormKey;
	private Data[] taskFormData=new Data[]{};
	private String taskFormFields;
	private String validFormFieldUrl;
	
	private boolean isCompleted = false;

	@JsonIgnore
	private String engineTaskId;	
	
	@JsonIgnore
	private long start;
	
	@JsonIgnore
	private long finish;
	
	
	
	public AxiApplicationTask(){
		super();
	}
	
		
	public AxiApplicationTask(int id,String formKey,String taskId){
		this.taskNumber=(new StringBuffer().append(id)).toString();
		this.taskFormKey=formKey;
		this.engineTaskId=taskId;		
	}
	public AxiApplicationTask(String id,String formKey,String taskId,String formFields){
		this.taskNumber=id;
		this.engineTaskId=taskId;
		this.taskFormKey=formKey;
		//this.formFields=Utils.parseBPMNMapToCompressedString( BPMNEngineProxyService.instance().getTaskVariables(taskId));
		this.taskFormFields=formFields;
	}
	
	
	

	public String getTaskNumber() {
		return taskNumber;
	}
	public void setTaskNumber(String id) {
		this.taskNumber = id;
	}
 

	public Data[] getTaskFormData() {
		return taskFormData;
	}
 

	public void setTaskFormData(Data[] formData) {
		this.taskFormData = formData;
	}
 
	
	public String getTaskFormKey() {
		return taskFormKey;
	}
	public void setTaskFormKey(String formKey) {
		this.taskFormKey = formKey;
	}
	
	
	@JsonIgnore
	public String getTaskId() {
		return engineTaskId;
	}
	@JsonIgnore
	public void setTaskId(String taskId) {
		this.engineTaskId = taskId;
	}
	

	public boolean isCompleted() {
		return isCompleted;
	}
	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public long setFinish() {
		return finish;
	}
	public void setFinish(long finish) {
		this.finish = finish;
	}

	
	
	
	public String getValidFormFieldUrl() {
		return validFormFieldUrl;
	}


	public void setValidFormFieldUrl(String validFormFieldUrl) {
		this.validFormFieldUrl = validFormFieldUrl;
	}


	@JsonIgnore
	public long getStart() {
		return start;
	}
	@JsonIgnore
	public void setStart(long start) {
		this.start = start;
	}
	
	
	public Map<String, String> getFormFields() {
		//return Utils.parseStringToMap(this.formFields);
		return Utils.parseJSONStringToMap(this.taskFormFields);
	}
	
	@JsonIgnore
	public String getFormFieldsAsJSONString() {
		//return Utils.parseStringToMap(this.formFields);
		return  this.taskFormFields;
	}
	
	
	public void setFormFields(Map<String, String> data) {		
		//this.formFields = Utils.parseMapToCompressedString(data);
		this.taskFormFields =Utils.parseMapToJSONString(data) ;				
	}
	
	
	/**
	 * 
	 * @param data
	 */
	public void update(Map<String, String> data) {
		Map<String,String> updatedMap = getFormFields();
		for(String key:data.keySet()){
			if(!Utils.isEmpty(data.get(key))){
				updatedMap.put(key, data.get(key));
			}
		}
		setFormFields(updatedMap);
	}
	
	
	

}
