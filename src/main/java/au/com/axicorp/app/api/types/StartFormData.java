package au.com.axicorp.app.api.types;

import java.util.HashMap;
import java.util.Map;

import au.com.axicorp.utils.Utils;

public class StartFormData {
	private Data[] data = new Data[] {};
	private PageInfo pageInfo = new PageInfo();
	private TrackingInfo trackingInfo = new TrackingInfo();
	private AdditionalInfo additionalInfo = new AdditionalInfo();

	public StartFormData() {
		super();
	}

	/**
	 * 
	 * @param requestMap
	 */
	public StartFormData(Map<String, String> requestMap) {
		this.pageInfo.setReferingUrl(requestMap.remove("referingUrl"));
		this.pageInfo.setBrand(requestMap.remove("brand"));
		this.pageInfo.setProductType(requestMap.remove("productType"));
		this.pageInfo.setLanguage(requestMap.remove("language"));
		this.pageInfo.setFormType(requestMap.remove("formType"));
		this.pageInfo.setFormId(requestMap.remove("formId"));
		this.pageInfo.setPartnerCode(requestMap.remove("partnerCode"));

		this.trackingInfo.setTracker(requestMap.remove("tracker"));
		this.trackingInfo.setVisitorId(requestMap.remove("visitorId"));
		this.trackingInfo.setCampaign(requestMap.remove("campaign"));
		this.trackingInfo.setSource(requestMap.remove("source"));
		this.trackingInfo.setMedium(requestMap.remove("medium"));
		this.trackingInfo.setTerm(requestMap.remove("term"));
		this.trackingInfo.setContent(requestMap.remove("content"));
		
		this.additionalInfo.setAdditionalInfo(requestMap);

	}

	/**
	 * Returns the Data Part as a Map
	 * 
	 * @return
	 */
	public Map<String, String> getDataMap() {
		Map<String, String> dataMap = new HashMap<String, String>();
		// Add the Data
		if (this.data != null) {
			for (int i = 0; i < data.length; i++) {
				if (data[i] != null && !Utils.isEmpty(data[i].getFieldName())) {
					dataMap.put(data[i].getFieldName(), data[i].getFieldValue());
				}
			}
		}

		return dataMap;
	}

	/**
	 * Returns the Meta-Data As A Map
	 * 
	 * @return
	 */
	public Map<String, String> getMetadataMap() {
		Map<String, String> m = new HashMap<String, String>();

		// Add the Page Info
		if (this.pageInfo != null) {
			m.put("referingUrl", pageInfo.getReferingUrl());
			m.put("brand", pageInfo.getBrand());
			m.put("productType", pageInfo.getProductType());
			m.put("language", pageInfo.getLanguage());
			m.put("formType", pageInfo.getFormType());
			m.put("formId", pageInfo.getFormId());
			m.put("partnerCode", pageInfo.getPartnerCode());
		}

		if (this.trackingInfo != null) {
			m.put("tracker", trackingInfo.getTracker());
			m.put("visitorId", trackingInfo.getVisitorId());
			m.put("campaign", trackingInfo.getCampaign());
			m.put("source", trackingInfo.getSource());
			m.put("medium", trackingInfo.getMedium());
			m.put("term", trackingInfo.getTerm());
			m.put("content", trackingInfo.getContent());
		}

		if (this.additionalInfo != null) {
			for (String key : additionalInfo.getAdditionalInfo().keySet()) {
				if (!Utils.isEmpty(additionalInfo.getAdditionalInfo().get(key)))
					m.put(key, additionalInfo.getAdditionalInfo().get(key));
			}
		}
		return m;
	}

	
	@SuppressWarnings("unused")
	public boolean isValidMetadata() {
		if(true)
			return true;
		if (this.pageInfo != null) {
			if (Utils.isEmpty(this.pageInfo.getReferingUrl()))
				return false;
			if (Utils.isEmpty(this.pageInfo.getBrand()))
				return false;
			if (Utils.isEmpty(this.pageInfo.getProductType()))
				return false;
			if (Utils.isEmpty(this.pageInfo.getLanguage()))
				return false;
			if (Utils.isEmpty(this.pageInfo.getFormType()))
				return false;
			if (Utils.isEmpty(this.pageInfo.getFormId()))
				return false;
			if (Utils.isEmpty(this.pageInfo.getPartnerCode()))
				return false;
		} else {
			return false;
		}

		if (this.trackingInfo != null) {
			if (Utils.isEmpty(this.trackingInfo.getTracker()))
				return false;
			if (Utils.isEmpty(this.trackingInfo.getVisitorId()))
				return false;
			if (Utils.isEmpty(this.trackingInfo.getCampaign()))
				return false;
			if (Utils.isEmpty(this.trackingInfo.getSource()))
				return false;
			if (Utils.isEmpty(this.trackingInfo.getMedium()))
				return false;
			if (Utils.isEmpty(this.trackingInfo.getTerm()))
				return false;
			if (Utils.isEmpty(this.trackingInfo.getContent()))
				return false;
		} else {
			return false;
		}

		return true;
	}

	public Data[] getData() {
		return data;
	}

	public void setData(Data[] data) {
		this.data = data;
	}

	public PageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}

	public TrackingInfo getTrackingInfo() {
		return trackingInfo;
	}

	public void setTrackingInfo(TrackingInfo trackingInfo) {
		this.trackingInfo = trackingInfo;
	}

	public AdditionalInfo getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(AdditionalInfo additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public static StartFormData generateSampleData() {
	
		Map<String,String> requestMap = new HashMap<String,String>();
		requestMap.put("referingUrl","www.axitrader.com/blog/123?source=google");
		requestMap.put("brand","AxiTraderAU");
		requestMap.put("productType","Retail");
		requestMap.put("language","EN");
		requestMap.put("formType","NonDemo");
		requestMap.put("formId","form_1716");
		requestMap.put("partnerCode","");

		requestMap.put("tracker","Marketo");
		requestMap.put("visitorId","id:287-GTJ-838%26token:_mch-marketo.com-1396310362214-46169");
		requestMap.put("campaign","free200credit");
		requestMap.put("source","Google");
		requestMap.put("medium","adwords");
		requestMap.put("term","mt4+broker");
		requestMap.put("content","");
		requestMap.put("promoCode","legacy_DH_blah");
 
		return new StartFormData(requestMap);	 
	}

}
