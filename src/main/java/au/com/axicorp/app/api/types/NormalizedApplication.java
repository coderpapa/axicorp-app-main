package au.com.axicorp.app.api.types;

import java.util.HashMap;
import java.util.Map;

public class NormalizedApplication {
	Map<String, String> data = new HashMap<String, String>();

	public NormalizedApplication(AxiApplication application) {
		Map<String, AxiApplicationTask> map = application.getFormByStepMap();
		for (String key : map.keySet()) {
			if (map.get(key) != null) {
				data.put(map.get(key).getTaskFormKey(), map.get(key).getFormFieldsAsJSONString());
			}
		}
	}

	public Map<String, String> getData() {
		return data;
	}

	public void setData(Map<String, String> data) {
		this.data = data;
	}

}
