package au.com.axicorp.app.api.types;

import java.util.HashMap;
import java.util.Map;

public class AdditionalInfo {
	private Map<String,String> additionalInfo = new HashMap<String,String>();
	
	public AdditionalInfo(){
		super();
	}

	public Map<String, String> getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(Map<String, String> additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	
	
}
