package au.com.axicorp.app.api;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.camunda.bpm.engine.impl.util.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;

import au.com.axicorp.app.api.types.AxiApplication;
import au.com.axicorp.app.api.types.AxiApplicationTask;
import au.com.axicorp.app.api.types.LoginRequest;
import au.com.axicorp.app.api.types.NormalizedApplication;
import au.com.axicorp.app.api.types.StartFormData;
import au.com.axicorp.app.exceptions.InvalidLoginPin;
import au.com.axicorp.app.exceptions.InvalidSessionToken;
import au.com.axicorp.app.exceptions.InvalidStartFormDataException;
import au.com.axicorp.app.service.ApplicationService;
import au.com.axicorp.app.service.SessionTokenService;
import au.com.axicorp.utils.Constants;
import au.com.axicorp.utils.Utils;

@Path("applications")
public class Applications {
	private Logger sLogger = Logger.getLogger(Applications.class.getName());
	@Context
	UriInfo uriInfo;



	/**
	 * Query Applications
	 * 
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<AxiApplication> getApplications() {
		return ApplicationService.instance().getApplications();
	}
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//  PATH:  /api/applications/
	//  Method: POST
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	/**
	 * Create New Application
	 * 
	 * @param requestMap
	 * @return
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createApplication(StartFormData data) {
		sLogger.log(Level.INFO, " Request: " + new JSONObject(data));

		try {
			AxiApplication application = ApplicationService.instance().createApplication(data);
			UriBuilder ub = uriInfo.getAbsolutePathBuilder();
			URI applicationURI = ub.path(application.getApplicationId()).build();
			Response response = Response.created(applicationURI).entity(application).build();
			response.getHeaders().add(Constants.X_AXIAPP_SESSION_TOKEN_KEY, application.getApplicationId() + "-1111");
			return response;
		} catch (InvalidStartFormDataException e) {
			sLogger.log(Level.INFO,
					" Failed to create Application from Request -  " + new JSONObject(data) + " " + e.getMessage());
			return Response.status(Status.BAD_REQUEST).build();
		}
	}
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//  PATH:  /api/applications/
	//  Method: GET
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	/**
	 * Query By Application Id
	 * 
	 * @param applicationId
	 * @return
	 */
	@GET
	@Path("/{applicationId}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	public Response getApplication(@HeaderParam(Constants.X_AXIAPP_SESSION_TOKEN_KEY) String sessionToken,
			@PathParam("applicationId") String applicationId) {
		checkSessionToken(applicationId,sessionToken);	 
		AxiApplication application = ApplicationService.instance().getApplicationById(applicationId);
		return Response.ok().entity(application).build();
	}
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//  PATH:  /api/applications/{id}/currentTask
	//  Method: POST
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	/**
	 * Update By Application Id and Submit
	 * 
	 * @param applicationId
	 * @param requestMap
	 * @return
	 */
	@POST
	@Path("/{applicationId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes("application/json")
	public Response updateApplicationAndSubmit(@HeaderParam(Constants.X_AXIAPP_SESSION_TOKEN_KEY) String sessionToken,
			@PathParam("applicationId") String applicationId, Map<String, String> requestMap) {

		checkSessionToken(applicationId,sessionToken);	 
		AxiApplication app = ApplicationService.instance().updateApplicationWithAction(applicationId, requestMap);
		return Response.ok().entity(app).build();
	}
	
	
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//  PATH:  /api/applications/{id}/currentTask
	//  Method: PUT
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	/**
	 * Update CurrenTask by Application ID
	 * 
	 * @param applicationId
	 * @param requestMap
	 * @return
	 */
	@PUT
	@Path("/{applicationId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes("application/json")
	public Response updateApplicationTaskAndSave(@HeaderParam(Constants.X_AXIAPP_SESSION_TOKEN_KEY) String sessionToken,
			@PathParam("applicationId") String applicationId, Map<String, String> requestMap) {
		checkSessionToken(applicationId,sessionToken);	 
		AxiApplication app = ApplicationService.instance().updateApplicationAndSave(applicationId, requestMap);	
		return Response.ok().entity(app).build();
	}

	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//  PATH:  /api/applications/{id}/login
	//  Method: POST
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	/**
	 * Login using HTTP POST & JSON Object
	 * 
	 * @param applicationId
	 * @return
	 */
	@POST
	@Path("/{applicationId}/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	public Response login(@PathParam("applicationId") String applicationId, Map<String, String> request) {
		if (request != null && request.get("pin") != null) {
			String pin = request.get("pin");
			return ApplicationService.instance().login(applicationId, pin);
		} else {
			throw new InvalidLoginPin(
					"No pin provided. Please provide a valid pin in the following manner - Sample JSON: {\"pin\":\"n-digit-pin\"} ");
		}
	}

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//  PATH:  /api/applications/{id}/login?pin
	//  Method: GET
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	/**
	 * Login using HTTP GET & Query Param
	 * 
	 * @param applicationId
	 * @return
	 */
	@GET
	@Path("/{applicationId}/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	public Response loginGet(@PathParam("applicationId") String applicationId) {
		String pin = uriInfo.getQueryParameters().getFirst("pin");
		if (!Utils.isEmpty(pin)) {
			return ApplicationService.instance().login(applicationId, pin);
		} else {
			throw new InvalidLoginPin(
					"No pin provided. Please provide a valid pin in the following manner - Sample JSON: {\"pin\":\"n-digit-pin\"} ");
		}
	}
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//  PATH:  /api/applications/{id}/currentTask
	//  Method: PUT
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	/**
	 * Update CurrenTask by Application ID
	 * 
	 * @param applicationId
	 * @param requestMap
	 * @return
	 */
	@PUT
	@Path("/{applicationId}/currentTask")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes("application/json")
	public Response updateApplicationTaskAndSaveCurrentTask(@HeaderParam(Constants.X_AXIAPP_SESSION_TOKEN_KEY) String sessionToken,
			@PathParam("applicationId") String applicationId, Map<String, String> requestMap) {

		checkSessionToken(applicationId,sessionToken);	 
		AxiApplication app = ApplicationService.instance().updateApplicationAndSave(applicationId, requestMap);
		AxiApplicationTask task = app.getCurrentTaskForm();
		return Response.ok().entity(task).build();
	}


	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//  PATH:  /api/applications/{id}/currentTask
	//  Method: POST
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	
	/**
	 * Update By Application Id and Submit
	 * 
	 * @param applicationId
	 * @param requestMap
	 * @return
	 */
	@POST
	@Path("/{applicationId}/currentTask")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes("application/json")
	public Response updateApplicationAndSubmitCurrentTask(@HeaderParam(Constants.X_AXIAPP_SESSION_TOKEN_KEY) String sessionToken,
			@PathParam("applicationId") String applicationId, Map<String, String> requestMap) {
		checkSessionToken(applicationId,sessionToken);	 
		AxiApplication app = ApplicationService.instance().updateApplicationWithAction(applicationId, requestMap);
		AxiApplicationTask task = app.getCurrentTaskForm();
		return Response.ok().entity(task).build();
	}
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//  PATH:  /api/applications/examples/startformdata
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	@GET
	@Path("/examples/startformdata")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getExampleStartFormData() {
		StartFormData d = StartFormData.generateSampleData();
		return Response.ok().entity(d).build();
	}
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//  PATH:  /api/applications/{id}/normalized
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	/**
	 * Get Normalized Application
	 * 
	 * @param applicationId
	 * @return
	 */
	@GET
	@Path("/{applicationId}/normalized")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getApplicationNormalized(@PathParam("applicationId") String applicationId) {
		AxiApplication application = ApplicationService.instance().getApplicationById(applicationId);
		NormalizedApplication n = new NormalizedApplication(application);
		return Response.ok().entity(n).build();
	}

	
	private void checkSessionToken(String applicationId,String sessionToken){
		if (Utils.isEmpty(sessionToken)
				|| !SessionTokenService.instance().isValidSessionId(applicationId, sessionToken)) {
			UriBuilder ub = uriInfo.getAbsolutePathBuilder();
			URI loginURI = ub.path("/login").build();
			sLogger.log(Level.INFO, " Failed to validation session token, [ " + Constants.X_AXIAPP_SESSION_TOKEN_KEY
					+ ", " + sessionToken + "]");
			throw new InvalidSessionToken("Invalid Session Token. Please login", loginURI);
		}
	}

}
