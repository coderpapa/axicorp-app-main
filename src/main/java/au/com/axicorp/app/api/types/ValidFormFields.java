package au.com.axicorp.app.api.types;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.camunda.bpm.engine.impl.util.json.JSONArray;
import org.json.JSONObject;

import au.com.axicorp.utils.Utils;

public class ValidFormFields {
	private List<ValidFormField> validFields = new ArrayList<ValidFormField>();

	public ValidFormFields() {
		super();
	}

	public void add(String fieldName, String type, boolean isRequired, String description) {
		validFields.add(new ValidFormField(fieldName, type, isRequired, description));		 
	}

	public List<ValidFormField> getValidFields() {
		return validFields;
	}

	public void setValidFields(List<ValidFormField> validFields) {
		this.validFields = validFields;
	}
	
	public String toString(){	 	
		return new JSONObject(this).toString();
	}

	/**
	 * Compare incoming Map of <FormFieldName,FormFieldValue> with the 
	 *   Form Field "Names" in the List of Valid Fields and if *ANY*
	 *   is missing then return false.
	 *   
	 * Otherwise return true.
	 *   
	 * @param inputFormFields
	 * @return
	 */
	public boolean hasRequiredFields(Map<String, String> inputFormFields) {
		for (ValidFormField field : getValidFields()) {
			String value = inputFormFields.get(field.getFieldName());
			if(Utils.isEmpty(value)){
				return false;
			}
		}
		return true;
	}

	public class ValidFormField {
		private String fieldName;
		private String type;
		private boolean isRequired;
		private String description;

		public ValidFormField() {
			super();
		}

		public ValidFormField(String name, String type, boolean isRequired, String description) {
			super();
			this.fieldName = name;
			this.type = type;
			this.isRequired = isRequired;
			this.description = description;

		}
		
		public String toString(){
			return (new JSONObject(this)).toString(); 
		}

		public String getFieldName() {
			return fieldName;
		}

		public void setFieldName(String fieldName) {
			this.fieldName = fieldName;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public boolean isRequired() {
			return isRequired;
		}

		public void setRequired(boolean isRequired) {
			this.isRequired = isRequired;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

	}

}