package au.com.axicorp.app.api.types;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonIgnore;

import au.com.axicorp.bpmn.services.BPMNEngineProxyService;
import au.com.axicorp.utils.Constants;
import au.com.axicorp.utils.ConfigPropertyService;
import au.com.axicorp.utils.Utils;

public class AxiApplication {

	private int currentStep = -1;

	@JsonIgnore
	static Logger sLogger = Logger.getLogger(AxiApplication.class.getName());

	private String applicationId;
	private String currentTaskNumber;
	private String currentTaskURL;
	private String status;
	private String currentTaskFormKey;
	private String processInstanceId;
	private AxiApplicationTask[] taskFormDataArray = new AxiApplicationTask[] {};
	private Map<String, AxiApplicationTask> formByStepMap = new TreeMap<String, AxiApplicationTask>();
	private Map<String, String> metadata = new HashMap<String,String>();
	
	@JsonIgnore
	private String pin ;


	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public AxiApplication() {
		super();
	}

	public String getCamundaProcessId() {
		return processInstanceId;
	}

	public void setCamundaProcessId(String camundaProcessId) {
		processInstanceId = camundaProcessId;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCurrentFormKey() {
		return currentTaskFormKey;
	}

	public void setCurrentFormKey(String currentformKey) {
		this.currentTaskFormKey = currentformKey;
	}

	public String getCurrentTaskNumber() {
		return currentTaskNumber;
	}

	public void setCurrentTaskNumber(String currentTaskId) {
		this.currentTaskNumber = currentTaskId;
	}

	public String getCurrentTaskURL() {
		return currentTaskURL;
	}

	public void setCurrentTaskURL(String currentTaskURL) {
		this.currentTaskURL = currentTaskURL;
	}

	public Map<String, AxiApplicationTask> getFormByStepMap() {
		return formByStepMap;
	}

	public void setFormByStepMap(Map<String, AxiApplicationTask> formByStepMap) {
		this.formByStepMap = formByStepMap;
	}

	public AxiApplicationTask[] getTaskFormDataArray() {
		return taskFormDataArray;
	}

	public void setTaskFormDataArray(AxiApplicationTask[] formByStepMap) {
		this.taskFormDataArray = formByStepMap;
	}

	public int getCurrentStep() {
		return currentStep;
	}

	public void setCurrentStep(int currentStep) {
		this.currentStep = currentStep;
	}

	public String getCurrentTaskFormKey() {
		return currentTaskFormKey;
	}

	public void setCurrentTaskFormKey(String currentTaskFormKey) {
		this.currentTaskFormKey = currentTaskFormKey;
	}

	/**
	 * 1. Invoke Camunda Process Engine Complete Task with the current form id
	 * and values 2. Get the Next Task Id and Form Key 3. Set the Application
	 * Status based on the next form key
	 * 
	 */
	public AxiApplicationTask createNewFormEntry() {
		AxiApplicationTask form = new AxiApplicationTask(getCurrentStep(), getCurrentFormKey(), getCurrentTaskNumber());
		Map<String, String> vars = BPMNEngineProxyService.instance().getDefaultVarMapByTaskId(getCurrentTaskNumber());
		form.setFormFields(vars);
		formByStepMap.put(getCurrentStepAsString(), form);
		return form;
	}

	/**
	 * Take incoming Key/Value pair data and upsert into the Form that is the
	 * current form
	 * 
	 * @param data
	 */
	public void updateCurrentForm(Map<String, String> data) {
		sLogger.log(Level.INFO, "   [updateCurrentForm] Add data: " + new JSONObject(data) + " to formByStepMap("
				+ getCurrentStepAsString() + ")");
		if (getStatus().equals(Constants.COMPLETED))
			return;
		AxiApplicationTask currentForm = formByStepMap.get(getCurrentStepAsString());
		if (currentForm == null) {
			currentForm = createNewFormEntry();
		}
		currentForm.update(data);
	}

	public AxiApplicationTask updateFormByFormKey(String formKey, Map<String, String> data) {
		if (Utils.isEmpty(formKey))
			return null;
		for (String key : formByStepMap.keySet()) {
			AxiApplicationTask f = formByStepMap.get(key);
			if (f != null && f.getTaskFormKey().trim().toLowerCase().equals(formKey.trim().toLowerCase())) {
				f.update(data);
				return f;
			}
		}
		return null;
	}

	public void incrementStep() {
		this.currentStep++;
	}

	@JsonIgnore
	private String getCurrentStepAsString() {
		return new StringBuffer().append(getCurrentStep()).toString();
	}

	public boolean isValidPin(String pin2) {
		if (this.pin.trim().equals(pin2)) {
			return true;
		}
		return false;
	}

	public AxiApplicationTask getCurrentTaskForm() {
		return formByStepMap.get(currentTaskFormKey);
	}

	public void setApplicationMetadata(Map<String, String> startRequestMetadata) {
		this.metadata = startRequestMetadata;
	}

	public Map<String, String> getApplicationMetadata() {
		return this.metadata;
	}

}
