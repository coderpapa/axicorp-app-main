package au.com.axicorp.app.api.types;

public class PageInfo {
    private String referingUrl;
    private String brand;
    private String productType;
    private String language;
    private String formType;
    private String formId;
    private String partnerCode;
    
    public PageInfo(){
    	super();
    }
	public String getReferingUrl() {
		return referingUrl;
	}
	public void setReferingUrl(String referingUrl) {
		this.referingUrl = referingUrl;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}
	public String getFormId() {
		return formId;
	}
	public void setFormId(String formId) {
		this.formId = formId;
	}
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
    

}
