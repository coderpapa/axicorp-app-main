package au.com.axicorp.app.api.types;

public class ApplicationLoginRequest {
	private String pin;

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}
	
}
