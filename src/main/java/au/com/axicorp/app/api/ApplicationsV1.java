package au.com.axicorp.app.api;

import java.net.URI;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import au.com.axicorp.app.api.types.AxiApplication;
import au.com.axicorp.app.api.types.StartFormData;
import au.com.axicorp.app.exceptions.InvalidStartFormDataException;
import au.com.axicorp.app.service.ApplicationService;

@Path("/v1/applications")
public class ApplicationsV1 {
	@Context
    UriInfo uriInfo;
	
	/**
	 * Query Applications
	 * 
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<AxiApplication> getApplications() {
		return ApplicationService.instance().getApplications();
	}

	
	/**
	 * Create New Application
	 * 
	 * @param requestMap
	 * @return
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes("application/json")
	public Response createApplication(StartFormData startFormData) {		
		try {
			AxiApplication application = ApplicationService.instance().createApplication(startFormData);			 	 
			UriBuilder ub = uriInfo.getAbsolutePathBuilder();
            URI applicationURI = ub.
                    path(application.getApplicationId()).
                    build();
			return Response.created(applicationURI).entity(application).build();
		} catch (InvalidStartFormDataException e) {
			 return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
	}
	/**
	 * Query By Application Id
	 * 
	 * @param applicationId
	 * @return
	 */
	@GET
	@Path("/{applicationId}")
	@Produces(MediaType.APPLICATION_JSON)
	public AxiApplication getApplication(@PathParam("applicationId") String applicationId) {
		return ApplicationService.instance().getApplicationById(applicationId);
	}

	/**
	 * Update By Application Id and Submit
	 * 
	 * @param applicationId
	 * @param requestMap
	 * @return
	 */
	@PUT
	@Path("/{applicationId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes("application/json")
	public AxiApplication updateApplicationAndSave(@PathParam("applicationId") String applicationId,
			Map<String, String> requestMap) {
		return ApplicationService.instance().updateApplicationAndSave(applicationId, requestMap);
	}

	/**
	 * Update By Application Id and Submit
	 * 
	 * @param applicationId
	 * @param requestMap
	 * @return
	 */
	@POST
	@Path("/{applicationId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes("application/json")
	public AxiApplication updateApplicationAndSubmit(@PathParam("applicationId") String applicationId,
			Map<String, String> requestMap) {
		return ApplicationService.instance().updateApplicationWithAction(applicationId, requestMap);
	}
	
	
	

	/**
	 * Update By Application Id and Submit
	 * 
	 * @param applicationId
	 * @param requestMap
	 * @return
	 */
	@PUT
	@Path("/{applicationId}/{formKey}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes("application/json")
	public AxiApplication updateFormByApplication(@PathParam("applicationId") String applicationId,@PathParam("formKey") String formKey,
			Map<String, String> requestMap) {
		return ApplicationService.instance().updateForm(applicationId, formKey,requestMap);
	}
	
	
	public static void main(String [] args){
		Map<String,String> data = new HashMap<String,String>();
		data.put("lastName", "frost");
		new ApplicationsV1().updateApplicationAndSave("29121887-9d3e-4f03-afb7-7d7b012cde82",data);
	}
	
	
	

}


