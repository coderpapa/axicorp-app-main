package au.com.axicorp.app.api;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import au.com.axicorp.app.api.types.AxiApplication;
import au.com.axicorp.app.api.types.ValidFormFields;
import au.com.axicorp.app.service.ApplicationService;
import au.com.axicorp.bpmn.services.BPMNModelService;
 

@Path("forms")
public class Forms {
	// /api/forms/{formKey}/validfields.json
	
	/**
	 * Query Applications
	 * 
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String[] getApplications() {
		return BPMNModelService.instance().getFormKeys();
	}
	

	/**
	 * Query By Form Key
	 * 
	 * @param applicationId
	 * @return
	 */
	@GET
	@Path("/{formKey}")
	@Produces(MediaType.APPLICATION_JSON)
	public ValidFormFields getApplication(@PathParam("formKey") String formKey) {
		return  BPMNModelService.instance().getFormFieldsByFormKey(formKey);
	}
	
}
