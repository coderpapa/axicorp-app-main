package au.com.axicorp.app.tests;

import java.util.Map;

import org.json.JSONObject;

import au.com.axicorp.app.api.types.AxiApplication;
import au.com.axicorp.app.api.types.StartFormData;
import au.com.axicorp.app.exceptions.InvalidStartFormDataException;
import au.com.axicorp.app.service.ApplicationService;
import au.com.axicorp.utils.Utils;

public class TestApplicationService {

	/**
	 * @throws InvalidStartFormDataException
	 * 
	 */
	public void runEndToEndTest() throws InvalidStartFormDataException {

		// Create Application
		AxiApplication app = ApplicationService.instance().createApplication(StartFormData.generateSampleData());
		
		// Update Basic Contact Details for
		Map<String,String> basicContactDetais = generateContactDetailsData();

	}

	private Map<String, String> generateContactDetailsData() {
		Map<String, String> data = Utils.parseJSONStringToMap("");
		return null;
	}

	public static void main(String[] args) {

	}
}
