package au.com.axicorp.app.exceptions;

public class InvalidStartFormDataException extends Exception {
	private String message;

	public InvalidStartFormDataException(String s){
		super();
		this.message=s;
	}
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
