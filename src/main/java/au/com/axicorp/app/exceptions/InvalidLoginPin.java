package au.com.axicorp.app.exceptions;

import java.net.URI;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class InvalidLoginPin extends WebApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidLoginPin(String message) {
		super(Response.status(401).entity(message).type("text/plain").header("WWW-Authenticate", message).build());

	}

}

