package au.com.axicorp.app.exceptions;

import java.net.URI;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class InvalidSessionToken extends WebApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidSessionToken(String message, URI loginURI) {
		super(Response.status(403).entity(message + ". " + loginURI.toString()).type("text/plain").link(loginURI, "/login").build());

	}

}
