package au.com.axicorp.app.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.runtime.ProcessInstance;

import au.com.axicorp.app.api.types.AxiApplication;
import au.com.axicorp.app.api.types.AxiApplicationTask;
import au.com.axicorp.app.api.types.StartFormData;
import au.com.axicorp.app.exceptions.InvalidLoginPin;
import au.com.axicorp.app.exceptions.InvalidStartFormDataException;
import au.com.axicorp.bpmn.services.BPMNEngineProxyService;
import au.com.axicorp.db.service.ApplicationDatabaseService;
import au.com.axicorp.utils.Constants;
import au.com.axicorp.utils.ConfigPropertyService;
import au.com.axicorp.utils.Utils;

public class ApplicationService {
	private static ApplicationService instance = null;
	private Logger sLogger = Logger.getLogger(ApplicationService.class.getName());

	private ApplicationService() {
	}

	/**
	 * 
	 * @return
	 */
	public static ApplicationService instance() {
		if (instance == null) {
			instance = new ApplicationService();
		}
		return instance;
	}

	/**
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	 
	 * CREATE Application from StartFormData
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 * 
	 * @param startFormDataASObject
	 * @return
	 * @throws InvalidStartFormDataException
	 */
	public AxiApplication createApplication(StartFormData startFormDataASObject) throws InvalidStartFormDataException {
		if (startFormDataASObject == null)
			throw new InvalidStartFormDataException("Start form object was empty");

		if (!startFormDataASObject.isValidMetadata())
			throw new InvalidStartFormDataException("Start form metadata is invalid ");

		String newApplicationID = Utils.generateApplicationID();

		Map<String, String> startRequest = startFormDataASObject.getMetadataMap();

		/**
		 * Create Object
		 */
		AxiApplication app = new AxiApplication();
		app.setApplicationId(Utils.generateApplicationID());
		app.setStatus(Constants.STARTED);
		startRequest.put(Constants.ApplicationId, newApplicationID);
	 
		/**
		 * Start Process
		 */
		String processId = BPMNEngineProxyService.instance().startProcess(startRequest).getProcessInstanceId();
		app.setCamundaProcessId(processId);
		app = setNextTaskDetails(app);

		/**
		 * Put Data in the Current Task
		 */
		Map<String, String> requestDataMap = startFormDataASObject.getDataMap();
		app.updateCurrentForm(requestDataMap);
		if (!Utils.isEmpty(app.getCurrentTaskNumber())) {
			BPMNEngineProxyService.instance().updateTask(app.getCurrentTaskNumber(), Utils.toObjectMap(requestDataMap));
		}

		/**
		 * Update Database
		 */
		ApplicationDatabaseService.instance().insertApplication(app);

		return app;
	}

	/**
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 * CREATE Application from Map<String, String>
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 * 
	 * @param freeFormKeyValuePairMap
	 * @return
	 * @throws InvalidStartFormDataException
	 */
	public AxiApplication createApplication(Map<String, String> freeFormKeyValuePairMap)
			throws InvalidStartFormDataException {
		if (freeFormKeyValuePairMap == null)
			throw new InvalidStartFormDataException("Start form object was empty");

		StartFormData startFormData = new StartFormData(freeFormKeyValuePairMap);
		if (!startFormData.isValidMetadata())
			throw new InvalidStartFormDataException("Start form metadata is invalid ");

		String newApplicationID = Utils.generateApplicationID();
		Map<String, String> requestDataMap = startFormData.getDataMap();
		Map<String, String> startRequest = startFormData.getMetadataMap();

		/**
		 * Create Object
		 */
		AxiApplication app = new AxiApplication();
		app.setApplicationId(Utils.generateApplicationID());
		app.setApplicationMetadata(startRequest);
		app.setStatus(Constants.STARTED);
		startRequest.put(Constants.ApplicationId, newApplicationID);

		/**
		 * Start Process
		 */
		String processId = BPMNEngineProxyService.instance().startProcess(startRequest).getProcessInstanceId();
		app.setCamundaProcessId(processId);
		app = setNextTaskDetails(app);

		/**
		 * Put Data in the Current Task
		 */
		app.updateCurrentForm(requestDataMap);
		if (!Utils.isEmpty(app.getCurrentTaskNumber())) {
			BPMNEngineProxyService.instance().updateTask(app.getCurrentTaskNumber(), Utils.toObjectMap(requestDataMap));
		}

		/**
		 * Update Database
		 */
		ApplicationDatabaseService.instance().insertApplication(app);

		return app;
	}

	/**
	 * Get ALL the Applications
	 * 
	 * @return
	 */
	public List<AxiApplication> getApplications() {
		return new ArrayList<AxiApplication>();
	}

	/**
	 * Get Application By Id
	 * 
	 * @param applicationId
	 * @return
	 */
	public AxiApplication getApplicationById(String applicationId) {
		if (Utils.isEmpty(applicationId))
			return new AxiApplication();
		return ApplicationDatabaseService.instance().queryApplicationById(applicationId);
	}

	/**
	 * Update Application 
	 * Add new data to current task in given Application Id
	 * 
	 * @param applicationId
	 * @param requestMap
	 * @return
	 */
	public AxiApplication updateApplicationAndSave(String applicationId, Map<String, String> requestMap) {
		// Return empty object for bad applicationId
		if (Utils.isEmpty(applicationId))
			return new AxiApplication();

		// Query existing Application by Id
		AxiApplication app = ApplicationDatabaseService.instance().queryApplicationById(applicationId);

		// Return empty object for App not found in DB
		if (app == null) {
			return new AxiApplication();
		}

		// If Empty Updata Data then return existing App
		if (requestMap == null || requestMap.keySet() == null) {
			return app;
		}

		// Update the Current Form with the Data
		app.updateCurrentForm(requestMap);

		if (!Utils.isEmpty(app.getCurrentTaskNumber())) {
			// Invoke Camunda API & Update Task
			BPMNEngineProxyService.instance().updateTask(app.getCurrentTaskNumber(), Utils.toObjectMap(requestMap));
		}

		// Persist the Application Object in the DB - UPDATE Application
		ApplicationDatabaseService.instance().updateApplication(app);

		// Respond back with the App
		return app;
	}

	/**
	 * Update Task Form 
	 * 
	 * @param applicationId
	 * @param formKey
	 * @param requestMap
	 * @return
	 */
	public AxiApplication updateForm(String applicationId, String formKey, Map<String, String> requestMap) {
		if (Utils.isEmpty(applicationId))
			return new AxiApplication();

		AxiApplication app = ApplicationDatabaseService.instance().queryApplicationById(applicationId);
		// Return empty object for App not found in DB
		if (app == null) {
			return new AxiApplication();
		}

		// If Empty Updata Data then return existing App
		if (requestMap == null || requestMap.keySet() == null) {
			return app;
		}

		AxiApplicationTask f = app.updateFormByFormKey(formKey, requestMap);
		if (f != null && !Utils.isEmpty(f.getTaskId())) {
			// Invoke Camunda API & Update Task
			BPMNEngineProxyService.instance().updateTask(f.getTaskId(), Utils.toObjectMap(requestMap));
		}

		return app;
	}

	/**
	 * Update Application with new Form Data
	 * 
	 * - If Submit: Complete current Task and Move ahead
	 * 
	 * @param applicationId
	 * @param requestMap
	 * @return
	 */
	public AxiApplication updateApplicationWithAction(String applicationId, Map<String, String> requestMap) {
		// Return empty object for bad applicationId
		if (Utils.isEmpty(applicationId))
			return new AxiApplication();

		// Determine ACTION from request
		String action = Constants.ACTION_SUBMIT;
		if (requestMap == null || requestMap.keySet() == null) {
			requestMap = new HashMap<String, String>();
		} else {
			if (!Utils.isEmpty(requestMap.get("ACTION"))) {
				action = requestMap.get("ACTION");
				requestMap.remove("ACTION"); // you only want the form data now
			}
		}

		// Query existing Application by Id
		AxiApplication app = ApplicationDatabaseService.instance().queryApplicationById(applicationId);
		if (app == null) {
			return new AxiApplication();
		}

		// Update the Data in current FORM
		app.updateCurrentForm(requestMap);

		// Handle Form Actions [Default is SUBMIT]
		if (Constants.ACTION_SUBMIT.equals(action) && !Utils.isEmpty(app.getCurrentTaskNumber())) {
			BPMNEngineProxyService.instance().completeTask(app.getCurrentTaskNumber(), Utils.toObjectMap(requestMap));
		}

		// Invoke Camunda API & Get Details of Next Step
		app = setNextTaskDetails(app);

		// Persist the Application Object in the DB - UPDATE Application
		ApplicationDatabaseService.instance().updateApplication(app);

		// Respond back with the App
		return app;
	}

	/**
	 * State Transition logic for the Application Object
	 * 
	 * - Pre: create or update has populated data to the FormMap using the
	 * currentForm Key
	 * 
	 * - Post: the Application's currentFormKey and currentTaskId are now
	 * 'incremented'
	 * 
	 * @param app
	 * @return
	 */
	private AxiApplication setNextTaskDetails(AxiApplication app) {

		// Get Next Task:
		// - This can be empty if there is no more tasks
		app.setCurrentTaskNumber(
				BPMNEngineProxyService.instance().getCurrentTaskByProcessId(app.getCamundaProcessId()));

		if (!Utils.isEmpty(app.getCurrentTaskNumber())) {
			// If Next Task is NOT Null:
			// 1. Get Next Form Key
			// 2. Increment Step Id
			// 3. Add new Empty Form
			String currentFormKey = BPMNEngineProxyService.instance().getFormKeyByTaskId(app.getCurrentTaskNumber());
			// if (Utils.different(currentFormKey, app.getCurrentFormKey())) {
			app.setCurrentFormKey(currentFormKey);
			app.incrementStep();
			app.createNewFormEntry();
			// }

		} else {
			// If Next Task is Null:
			// 1. Set the Application Object to Completed State
			app.setStatus(Constants.COMPLETED);
		}

		return app;
	}

	public Response login(String applicationId, String pin) {
		AxiApplication application = ApplicationService.instance().getApplicationById(applicationId);
		if (!Utils.isEmpty(pin)) {
			if (application.isValidPin(pin)) {
				String sessionToken = SessionTokenService.instance().createNewSession(application.getApplicationId());
				Response response = Response.ok().entity("User logged and new session token created ").build();
				response.getHeaders().add(Constants.X_AXIAPP_SESSION_TOKEN_KEY, sessionToken);
				return response;
			} else {
				throw new InvalidLoginPin("Invalid login pin " + pin + " for application id " + applicationId);
			}
		}
		return null;
	}

}
