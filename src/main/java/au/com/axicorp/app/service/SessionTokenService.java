package au.com.axicorp.app.service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import au.com.axicorp.utils.Utils;

public class SessionTokenService {
	private static SessionTokenService instance = null;
	private Logger sLogger = Logger.getLogger(SessionTokenService.class.getName());

	Map<String,String> db = new HashMap<String,String>();
	
	private SessionTokenService(){}
	public static SessionTokenService instance() {
		if (instance == null) {
			instance = new SessionTokenService();
		}
		return instance;
	}
	
	
	public String createNewSession(String applicationId) {
		String sessionId = UUID.randomUUID().toString();
		 //Call DB and update new value
		db.put(applicationId, sessionId);
		return sessionId;
	}
	
	
	public boolean isValidSessionId(String applicationId,String sessionId){
		String curSessionId=db.get(applicationId);
		if(!Utils.isEmpty(curSessionId) && curSessionId.equals(sessionId))
			return true;
		
		return false;
	}
	

}
