package au.com.axicorp.app.types;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SalesforceContactQueryResponse {

	private SFClientInfo salesForceClientInfo;

	public SalesforceContactQueryResponse(String body) {
		try {
			SFClientInfo[] objs=	new ObjectMapper().readValue(body, SFClientInfo[].class);			
			if(objs.length > 0){
				this.setSalesForceClientInfo(objs[0]);
			}
		} catch (JsonParseException e) {			 
			e.printStackTrace();
		} catch (JsonMappingException e) {
				e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public SFClientInfo getSalesForceClientInfo() {
		return salesForceClientInfo;
	}

	public void setSalesForceClientInfo(SFClientInfo salesForceClientInfo) {
		this.salesForceClientInfo = salesForceClientInfo;
	}

	public boolean isUserInSalesForce() {

		if (salesForceClientInfo != null && (salesForceClientInfo.getClientType().equals(SFClientInfo.ClientType.LEAD)
				|| salesForceClientInfo.getClientType().equals(SFClientInfo.ClientType.CONTACT))) {
			return true;
		}

		return false;
	}

	public boolean isExistingLead() {
		if (salesForceClientInfo != null && salesForceClientInfo.getClientType().equals(SFClientInfo.ClientType.LEAD))
			return true;
		return false;
	}

	public boolean isExistingContact() {
		if (salesForceClientInfo != null
				&& salesForceClientInfo.getClientType().equals(SFClientInfo.ClientType.CONTACT))
			return true;
		return false;
	}

	public boolean isIndividualAcct() {
		if (salesForceClientInfo != null
				&& salesForceClientInfo.getAccountType().equals(SFClientInfo.AccountType.INDIVIDUAL))
			return true;
		return false;
	}

	public boolean isJointAcct() {
		if (salesForceClientInfo != null
				&& salesForceClientInfo.getAccountType().equals(SFClientInfo.AccountType.JOINT))
			return true;
		return false;
	}

	public boolean isCompanyAccount() {
		if (salesForceClientInfo != null
				&& salesForceClientInfo.getAccountType().equals(SFClientInfo.AccountType.COMPANY))
			return true;
		return false;
	}


}
