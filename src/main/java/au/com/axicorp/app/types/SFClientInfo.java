package au.com.axicorp.app.types;
 

public class SFClientInfo {
	private ClientType clientType = ClientType.NOTFOUND;
	private AccountType accountType = AccountType.INDIVIDUAL;
	private Lead leadInfo = null;
	private Contact contactInfo = null;
			
	public SFClientInfo(){
		super();
	}
	
	
	
	public Lead getLeadInfo() {
		return leadInfo;
	}



	public void setLeadInfo(Lead leadInfo) {
		this.leadInfo = leadInfo;
	}



	public Contact getContactInfo() {
		return contactInfo;
	}



	public void setContactInfo(Contact contactInfo) {
		this.contactInfo = contactInfo;
	}



	public ClientType getClientType() {
		return clientType;
	}

	public void setClientType(ClientType clientType) {
		this.clientType = clientType;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public enum ClientType {
	    LEAD, CONTACT, NOTFOUND
	}
	
	public enum AccountType {
	    INDIVIDUAL, JOINT, COMPANY
	}
}
