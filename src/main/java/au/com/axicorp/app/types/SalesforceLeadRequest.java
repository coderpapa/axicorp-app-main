package au.com.axicorp.app.types;

public class SalesforceLeadRequest {
	private Lead lead;

	public Lead getLead() {
		return lead;
	}

	public void setLead(Lead lead) {
		this.lead = lead;
	}
	
}
