package au.com.axicorp.app.types;

import java.util.Map;

public class Lead {
	private String id;
	private Map<String,String> details;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Map<String, String> getDetails() {
		return details;
	}
	public void setDetails(Map<String, String> details) {
		this.details = details;
	}
	
	
	
}
