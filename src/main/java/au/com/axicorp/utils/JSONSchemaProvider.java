package au.com.axicorp.utils;

import java.io.IOException;

import org.json.JSONObject;

import au.com.axicorp.app.api.types.AxiApplicationTask;
import au.com.axicorp.app.api.types.LoginRequest;
import au.com.axicorp.app.api.types.AxiApplication;
import au.com.axicorp.app.api.types.StartFormData;
import au.com.axicorp.app.api.types.TaskFormData;
import au.com.axicorp.app.api.types.ValidFormFields;
import au.com.axicorp.form.constants.StaticFormVariables;
import pl.zientarski.SchemaMapper;

public class JSONSchemaProvider {

	private static String getSchema(Class  klass) {
		SchemaMapper mapper= new SchemaMapper();
		mapper.setRelaxedMode(true);
		JSONObject schema = mapper.toJsonSchema4(klass,true);
		return (schema.toString(4));
		
	}
	public static String getFormDataSchema(){
		return getSchema(StartFormData.class);
	}

	/**
	 * Generate Schema
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		//System.out.println(getFormDataSchema());
		System.out.println(getSchema(StaticFormVariables.class));
		//System.out.println(getSchema(ValidFormFields.class));
		//System.out.println(getSchema(ApplicationLoginRequest.class));
		//System.out.println(getSchema(TaskFormData.class));
		//System.out.println(getSchema(AxiApplicationTask.class));
		//System.out.println(getSchema(AxiApplication.class));
	}
	 


}
