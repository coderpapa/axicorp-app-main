package au.com.axicorp.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class Constants {
	
	private static final String SEP=System.getProperty("file.separator");
	private static final String propertyHome = System.getenv("CATALINA_HOME");  
	public static final String CONFIG_PROPS_FILE_NAME = propertyHome+SEP+"conf"+SEP+"applications-api.properties";
	public static final String CONFIG_PROPS_FILE_NAME_DESKTOP = "C:\\Users\\Alok.Mishra\\ApplyOnlineCode\\Camunda\\server\\apache-tomcat-7.0.62"+SEP+"conf"+SEP+"applications-api.properties";
	
	public static final String BPMN_FIELD_PROPS_FILE_NAME = propertyHome+SEP+"conf"+SEP+"bpmn-field.properties";
	public static final String BPMN_FIELD_PROPS_FILE_NAME_DESKTOP = "C:\\Users\\Alok.Mishra\\ApplyOnlineCode\\Camunda\\server\\apache-tomcat-7.0.62"+SEP+"conf"+SEP+"bpmn-api.properties";
	
	
	
	/**
	 * Key/Value Pair for BPMN File Name Property
	 */
	public static final String DEFAULT_BPMN_FILE_KEY = "bpmn-file";
	public static final String DEFAULT_BPMN_FILE_DEFAULT_VALUE = "META-INF\\application-flow-v1.bpmn";
 
	
	/**
	 *  Key/Value Pair for BPMN Flow Name Property
	 */
	public static final String PROCESS_DEF_KEY="process-definition";
	public static final String PROCES_DEF_DEFAULT_VALUE = "applyonline-v2";
	
	/**
	 *  Key/Value Pair for Salesforce API Property
	 */
	public static final String SALESFORCE_API_RESOURCE_KEY = "salesforce-api";
	public static final String SALESFORCE_API_RESOURCE_DEFAULT_VALUE = "http://localhost:8080/APIMock-0.0.1-SNAPSHOT/api/salesforce";
	
	/**
	 *  Key/Value Pair for EVP API Property
	 */
	public static final String EVP_API_RESOURCE_KEY= "evp-api";
	public static final String EVP_API_RESOURCE_DEFAULT_VALUE= "http://localhost:8080/APIMock-0.0.1-SNAPSHOT/api/verifications";
	
	/**
	 * Application STATES
	 */
	public static final String STARTING = "Starting";
	public static final String STARTED = "Started";
	public static final String RUNNING = "Running";
	public static final String COMPLETED = "Completed";
	public static final String SUSPENDED = "Suspended";
	
	/**
	 * Application Id Key
	 */
	public static final String ApplicationId = "applicationId";
	
	/**
	 * Screen Actions - default is SUBMIT
	 */
	public static final String ACTION_SUBMIT = "SUBMIT";
	public static final String START_FORM_KEY = "ProcessStartForm";
	public static final Map<String, Object> EMPTY_TASK_VARS = new HashMap<String,Object>();
	public static final String X_AXIAPP_SESSION_TOKEN_KEY = "X-AxiApp-Session-Token";
	
	

	public static Pattern[] patterns = new Pattern[]{
	        // Script fragments
	        Pattern.compile("<script>(.*?)</script>", Pattern.CASE_INSENSITIVE),
	        // src='...'
	        Pattern.compile("src[\r\n]*=[\r\n]*\\\'(.*?)\\\'", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
	        Pattern.compile("src[\r\n]*=[\r\n]*\\\"(.*?)\\\"", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
	        // lonely script tags
	        Pattern.compile("</script>", Pattern.CASE_INSENSITIVE),
	        Pattern.compile("<script(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
	        // eval(...)
	        Pattern.compile("eval\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
	        // expression(...)
	        Pattern.compile("expression\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
	        // javascript:...
	        Pattern.compile("javascript:", Pattern.CASE_INSENSITIVE),
	        // vbscript:...
	        Pattern.compile("vbscript:", Pattern.CASE_INSENSITIVE),
	        // onload(...)=...
	        Pattern.compile("onload(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL)
	    };
	 


}
