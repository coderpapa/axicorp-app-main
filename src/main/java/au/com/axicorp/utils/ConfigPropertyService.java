package au.com.axicorp.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConfigPropertyService {
	Logger sLogger = Logger.getLogger(ConfigPropertyService.class.getName());
	private static ConfigPropertyService instance = null;
	private static final boolean DYNAMIC_RELOAD = false;

	Properties p = null;

	public static ConfigPropertyService instance() {
		if (instance == null) {
			instance = new ConfigPropertyService();
		}
		return instance;
	}

	public Properties getProperties() {
		return getProperties(Constants.CONFIG_PROPS_FILE_NAME);
	}

	public Properties getProperties(String filename) {
		if (DYNAMIC_RELOAD || p == null) {
			p = new Properties();
			FileInputStream in;
			try {
				// p.load(PropertyService.class.getClassLoader().getResourceAsStream(Constants.PROPS_FILE_NAME));
				in = new FileInputStream(filename);
				sLogger.log(Level.INFO,
						"[getProperties()] Found properties file " + Constants.CONFIG_PROPS_FILE_NAME + " and loading ....");
				p.load(in);
				in.close();
			} catch (FileNotFoundException e) {
				if (filename.equals(Constants.CONFIG_PROPS_FILE_NAME)) {
					return getProperties(Constants.CONFIG_PROPS_FILE_NAME_DESKTOP);
				} else {
					sLogger.log(Level.SEVERE, " Cannot file properties file " + Constants.CONFIG_PROPS_FILE_NAME, e);
					p = getDefaultProperties();
				}
			} catch (IOException e) {
				sLogger.log(Level.SEVERE, " Cannot file properties file " + Constants.CONFIG_PROPS_FILE_NAME, e);
				p = getDefaultProperties();
			}
		}

		return p;

	}

	private Properties getDefaultProperties() {
		Properties p = new Properties();
		p.setProperty(Constants.PROCESS_DEF_KEY, Constants.PROCES_DEF_DEFAULT_VALUE);
		p.setProperty(Constants.SALESFORCE_API_RESOURCE_KEY, Constants.SALESFORCE_API_RESOURCE_DEFAULT_VALUE);
		p.setProperty(Constants.EVP_API_RESOURCE_KEY, Constants.EVP_API_RESOURCE_DEFAULT_VALUE);
		return p;
	}

	public String get(String key) {
		String value= getProperties().getProperty(key);
		if(Utils.isEmpty(value))
		{
			sLogger.log(Level.INFO, "   XXXXXXXXXXXXX Cannot file Property " + key);
			return key;
		}
		return value;
	}
}
