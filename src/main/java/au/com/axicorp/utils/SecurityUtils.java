package au.com.axicorp.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class SecurityUtils {
	public Map<String, String> removeXSSAndBadChars(Map<String, String> request) {
		Map<String, String> cleansedValues = new HashMap<String, String>();
		for (String key : request.keySet()) {
			String value = request.get(key);
			cleansedValues.put(stripXSS(key), stripXSS(value));
		}
		return cleansedValues;
	}

	private static String stripXSS(String value) {
		if (value != null) {
			// NOTE: It's highly recommended to use the ESAPI library and
			// uncomment the following line to
			// avoid encoded attacks.
			//value = ESAPI.encoder().canonicalize(value);

			// Avoid null characters
			value = value.replaceAll("\0", "");

			// Remove all sections that match a pattern
			for (Pattern scriptPattern : Constants.patterns) {
				value = scriptPattern.matcher(value).replaceAll("");
			}
		}
		return value;
	}

}
