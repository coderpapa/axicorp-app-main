package au.com.axicorp.utils;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.camunda.bpm.engine.impl.util.json.JSONObject;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Utils {
	private static Random r = new Random();
	
	public static boolean isEmpty(String key) {
		if (key == null || key.trim().equals(""))
			return true;
		else
			return false;
	}

	/**
	 * Convert Map<String,String> to Map<String,Oject>
	 * 
	 * @param requestMap
	 * @return
	 */
	public static Map<String, Object> toObjectMap(Map<String, String> requestMap) {
		if (requestMap == null || requestMap.keySet() == null || requestMap.keySet().size() == 0)
			return new HashMap<String, Object>();
		return Collections.<String, Object> unmodifiableMap(requestMap);
	}

	/**
	 * 1. Get the LUT & ensure the KeySet is in the list of valid values 2.
	 * Ensure the Key or Value is not some SQL injection or
	 * 
	 * @param requestMap
	 * @return
	 */
	public static boolean isValidRequest(Map<String, String> requestMap) {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * Compress a Map into a String, key/value pairs are separated by |
	 * String=key1,value1|key2,value2| .....
	 * 
	 * @param mapToCompress
	 * @return
	 */
	public static String parseMapToCompressedString(Map<String, String> mapToCompress) {
		Set<String> keys = mapToCompress.keySet();
		StringBuffer sb = new StringBuffer();
		for (String key : keys) {
			if (key != null) {
				sb.append(key.replaceAll("\\|", "").trim());
				sb.append(",");
				String value = mapToCompress.get(key);
				if (isEmpty(value))
					value = "";
				sb.append(value.replaceAll("\\|", "").trim());
				sb.append("|");
			}
		}
		return sb.toString();
	}

	/**
	 * De-Compress a String to a Map
	 * 
	 * @param compressedString
	 * @return
	 */
	public static Map<String, String> parseStringToMap(String compressedString) {
		if (isEmpty(compressedString))
			return new HashMap<String, String>();
		Map<String, String> retMap = new HashMap<String, String>();
		String[] items = compressedString.split("\\|");
		if (items != null && items.length > 0) {
			for (int i = 0; i < items.length; i++) {
				String[] pair = split(items[i], ",");
				if (pair != null) {
					retMap.put(pair[0], pair[1]);
				}
			}
		}
		return retMap;
	}

	@SuppressWarnings("unchecked")
	public static Map<String, String> parseJSONStringToMap(String compressed) {
		if (isEmpty(compressed))
			return new HashMap<String, String>();
		Map<String, String> result = new HashMap<String, String>();
		try {
			result = new ObjectMapper().readValue(compressed, HashMap.class);
		} catch (JsonParseException e) {

		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	
	@SuppressWarnings("unused")
	public static String parseMapToJSONString(Map<String, String> map){
		if(map == null )
			return "{}";
		JSONObject jo =new JSONObject(map);
		if(jo != null)
			return jo.toString();
		return "";
	}
	

	public static String parseBPMNMapToCompressedString(Map<String, Object> mapToCompress) {
		Set<String> keys = mapToCompress.keySet();
		StringBuffer sb = new StringBuffer();
		for (String key : keys) {
			if (key != null) {
				sb.append(key.replaceAll("\\|", "").trim());
				sb.append(",");
				String value = "";
				if (mapToCompress.get(key) != null)
					value = (String) mapToCompress.get(key);
				sb.append(value.replaceAll("\\|", "").trim());
				sb.append("|");
			}
		}
		return sb.toString();
	}

	/**
	 * Split a String by key
	 * 
	 * @param item
	 * @param splitKey
	 * @return
	 */
	private static String[] split(String item, String splitKey) {
		String[] arr = item.split(splitKey);
		if (arr != null) {
			if (arr.length > 1) {
				return arr;
			} else if (arr.length == 1) {
				return new String[] { arr[0], "" };
			}
		}
		return null;
	}

	public static String generateApplicationID() {
		return java.util.UUID.randomUUID().toString();
	}

	public static boolean different(String nextFormKey, String currentformKey) {
		if (Utils.isEmpty(currentformKey))
			return true;

		return currentformKey.equals(nextFormKey);
	}

	public static DataSource getDatasource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		dataSource.setUrl("jdbc:sqlserver://localhost:1433;dabaseName=Camunda");
		dataSource.setUsername("camunda");
		dataSource.setPassword("Welcome1");
		return dataSource;

	}

	public static String generateFourDigitPin() {
		
		StringBuffer sb = new StringBuffer();
		sb.append(r.nextInt(9));
		sb.append(r.nextInt(9));
		sb.append(r.nextInt(9));
		sb.append(r.nextInt(9));

		//return sb.toString();
		return "1234";
	}

}
