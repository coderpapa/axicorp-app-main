package au.com.axicorp.db.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import au.com.axicorp.app.api.types.AxiApplication;
import au.com.axicorp.app.api.types.AxiApplicationTask;
import au.com.axicorp.db.dao.JDBCAppTemplate;
import au.com.axicorp.db.dao.JDBCFormTemplate;
import au.com.axicorp.db.dao.JDBCFormVariableTemplate;
import au.com.axicorp.db.pojo.ApplicationFormTblRow;
import au.com.axicorp.db.pojo.ApplicationTblRow;
import au.com.axicorp.utils.Utils;

public class ApplicationDatabaseService {
	Logger sLogger = Logger.getLogger(ApplicationDatabaseService.class.getName());
	private static ApplicationDatabaseService instance = null;
	private JDBCAppTemplate appTemplate;
	private JDBCFormTemplate formTemplate;
	private JDBCFormVariableTemplate formvarTemplate;
	
	protected ApplicationDatabaseService() {
		super();
	}

	public static ApplicationDatabaseService instance() {
		if (instance == null) {
			instance = new ApplicationDatabaseService();
		}
		return instance;
	}

	/**
	 * Insert Application (Parent)
	 * 
	 * @param app
	 * @return
	 */
	public AxiApplication insertApplication(AxiApplication app) {
		if (app != null && app.getApplicationId() != null) {
			ApplicationTblRow applicationTableRowInstance = new ApplicationTblRow(app);
			int rowsAffected = getAppTemplate().insert(applicationTableRowInstance);
			if (rowsAffected > 0) {
				Map<String, AxiApplicationTask> forms = app.getFormByStepMap();
				for (String key : forms.keySet()) {
					AxiApplicationTask f = forms.get(key);
					insert(app.getApplicationId(), f);
				}
			}
		}
		return app;
	}

	/**
	 * Insert Form in an Application (Child)
	 * 
	 * @param applicationId
	 * @param f
	 */
	private void insert(String applicationId, AxiApplicationTask f) {
		if (f != null && applicationId != null) {
			ApplicationFormTblRow applicationFormTblRow = new ApplicationFormTblRow(applicationId, f);
			getFormTemplate().insert(applicationFormTblRow);
		}

	}

	/**
	 * Update Application (Parent)
	 * 
	 * @param app
	 * @return
	 */
	public AxiApplication updateApplication(AxiApplication app) {
		if (app != null && app.getApplicationId() != null) {
			ApplicationTblRow applicationTableRowInstance = getAppTemplate()
					.findByApplicationId(app.getApplicationId());
			if (applicationTableRowInstance != null) {
				applicationTableRowInstance.updateInstance(app);
				getAppTemplate().update(applicationTableRowInstance);
				Map<String, AxiApplicationTask> forms = app.getFormByStepMap();
				for (String key : forms.keySet()) {
					AxiApplicationTask f = forms.get(key);
					update(app.getApplicationId(), f);
				}
				
			}
		}
		return app;
	}
	
	

	private void update(String applicationId, AxiApplicationTask f) {
		if (f != null && applicationId != null) {
			ApplicationFormTblRow applicationFormTblRow = new ApplicationFormTblRow(applicationId, f);
			getFormTemplate().update(applicationId,applicationFormTblRow);
		}
		
	}

	/**
	 * Find Application By Id
	 * 
	 * @param applicationId
	 * @return
	 */
	public AxiApplication queryApplicationById(String applicationId) {
		ApplicationTblRow applicationTableRowInstance = getAppTemplate().findByApplicationId(applicationId);
		AxiApplication axiApp = applicationTableRowInstance.toAxiApplication();
		axiApp.setPin(getPin(applicationId));
		getFormsInApplicationByApplicationId(applicationId, axiApp);
		return axiApp;
	}

	private String getPin(String applicationId) {
		return Utils.generateFourDigitPin();
	}

	public void getFormsInApplicationByApplicationId(String applicationId,
			AxiApplication axiApp) {
		List<ApplicationFormTblRow> applicationTableRowInstances = getFormTemplate().findByApplicationId(applicationId);

		for (ApplicationFormTblRow applicationTableRowInstance : applicationTableRowInstances) {
			if (applicationTableRowInstance != null) {
				AxiApplicationTask axiAppForm = applicationTableRowInstance.toForm();
				if (axiAppForm != null)
					axiApp.getFormByStepMap().put(axiAppForm.getTaskNumber(), axiAppForm);
			}

		}
		 
	}
	
	public AxiApplicationTask findFormByApplicationIdAndFormKey(String applicationId,String formKey){
		ApplicationFormTblRow applicationTableRowInstance=getFormTemplate().findByApplicationIdAndFormKey(applicationId, formKey);
		AxiApplicationTask axiAppForm = applicationTableRowInstance.toForm();
		return axiAppForm;
	}

	/**
	 * 
	 * @return
	 */
	public JDBCAppTemplate getAppTemplate() {
		if (this.appTemplate == null) {
			this.appTemplate = new JDBCAppTemplate();
			this.appTemplate.setAppDataSource(Utils.getDatasource());
		}
		return appTemplate;
	}

	public void setAppTemplate(JDBCAppTemplate appTemplate) {
		this.appTemplate = appTemplate;
	}

	/**
	 * 
	 * @return
	 */
	public JDBCFormTemplate getFormTemplate() {
		if (this.formTemplate == null) {
			this.formTemplate = new JDBCFormTemplate();
			this.formTemplate.setAppDataSource(Utils.getDatasource());
		}
		return formTemplate;
	}

	public void setFormTemplate(JDBCFormTemplate formTemplate) {
		this.formTemplate = formTemplate;
	}

	public JDBCFormVariableTemplate getFormvarTemplate() {
		if (this.formvarTemplate == null) {
			this.formvarTemplate = new JDBCFormVariableTemplate();
			this.formvarTemplate.setAppDataSource(Utils.getDatasource());
		}
		return formvarTemplate;
	}

	public void setFormvarTemplate(JDBCFormVariableTemplate formvarTemplate) {
		this.formvarTemplate = formvarTemplate;
	}
	
	
	

}
