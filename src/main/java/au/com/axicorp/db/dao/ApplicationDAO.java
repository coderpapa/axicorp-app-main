package au.com.axicorp.db.dao;

import au.com.axicorp.db.pojo.ApplicationTblRow;

public interface ApplicationDAO {
	
	public int insert(ApplicationTblRow applicationTableRowInstance);
	public ApplicationTblRow findByApplicationId(String applicationId);
	public int update(ApplicationTblRow applicationTableRowInstance);
}
