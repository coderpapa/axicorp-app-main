package au.com.axicorp.db.dao;

import java.util.List;

import au.com.axicorp.db.pojo.ApplicationFormTblRow;

public interface FormDAO {
	public void insert(ApplicationFormTblRow applicationFormTblRow);

	public List<ApplicationFormTblRow> findByApplicationId(String applicationId);

	public void update(String applicationId, ApplicationFormTblRow applicationFormTblRow);
}
