package au.com.axicorp.db.dao;

import java.util.List;

import au.com.axicorp.db.pojo.ApplicationFormVariableTblRow;

 
public interface FormVariableDAO {
	public void insert(ApplicationFormVariableTblRow applicationFormTblRow);

	public List<ApplicationFormVariableTblRow> getAllRowsInFormVarsTable();
	
	public List<ApplicationFormVariableTblRow> getRowsInFormVarsTableByFormKey(String formKey);

	public void updateRowsInFormVarsTableById(String id, ApplicationFormVariableTblRow formVar);
	
	
}
