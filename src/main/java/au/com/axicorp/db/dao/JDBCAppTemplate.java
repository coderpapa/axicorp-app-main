package au.com.axicorp.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import au.com.axicorp.bpmn.services.CamundaStartupProcessor;
import au.com.axicorp.db.pojo.ApplicationTblRow;
import au.com.axicorp.db.service.ApplicationDatabaseService;
import au.com.axicorp.utils.Utils;

/**
 * 
 * @author alok.mishra
 *
 */
@Repository
public class JDBCAppTemplate implements ApplicationDAO {
	private Logger sLogger = Logger.getLogger(JDBCAppTemplate.class.getName());
	private static final String TABLE_NAME = "AXI_APP_DTL";
	
	// JDBC Template and SimpleJDBC Insert
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert insertApplication;
	@Autowired
	private DataSource appDataSource;

	/**
	 * 
	 */
	
	public JDBCAppTemplate() {
		super();

	}

	public int insert(ApplicationTblRow applicationTableRowInstance) {
		sLogger.log(Level.INFO, "ENTRY | insert() " + this.getAppDataSource().toString());
		sLogger.log(Level.INFO, "        Params: " + applicationTableRowInstance.toString());
		Map<String, Object> parameters = new HashMap<String, Object>(6);
		parameters.put("id", applicationTableRowInstance.getId());
		parameters.put("processDefinitionId", applicationTableRowInstance.getCamundaProcessId());
		parameters.put("currentStatus", applicationTableRowInstance.getCurrentApplicationStatus());
		parameters.put("currentFormKey", applicationTableRowInstance.getCurrentFormKey());
		parameters.put("currentTaskId", applicationTableRowInstance.getCurrentTaskId());
		parameters.put("currentStep", applicationTableRowInstance.getCurrentStep());
		int val = this.insertApplication.execute(parameters);
		sLogger.log(Level.INFO, "EXIT | insert() status=" + val);
		return val;
	}

	/**
	 * Find by ApplicationId
	 */
	public ApplicationTblRow findByApplicationId(String applicationId) {
		sLogger.log(Level.INFO, "ENTRY | findByApplicationId() ");
		sLogger.log(Level.INFO, "        Params: " + applicationId);
		String querySQL = "select id, processDefinitionId , currentStatus , currentFormKey , currentTaskId , currentStep   from AXI_APP_DTL where id= ?";
		ApplicationTblRow applicationRow = this.jdbcTemplate.queryForObject(querySQL, new Object[] { applicationId },
				new ApplicationRowMapperTbl());
		if (applicationRow != null) {
			sLogger.log(Level.INFO, "        Result: " + applicationRow);
		} else {
			applicationRow = new ApplicationTblRow();
			sLogger.log(Level.INFO, "        Result: null ");
		}
		sLogger.log(Level.INFO, "EXIT | findByApplicationId() ");
		return applicationRow;
	}

	/**
	 * Update by applicationId
	 */
	public int update(ApplicationTblRow applicationTableRowInstance) {
		sLogger.log(Level.INFO, "ENTRY | update() ");
		sLogger.log(Level.INFO, "        Params: " + applicationTableRowInstance);
		if(Utils.isEmpty(applicationTableRowInstance.getId())){
			return 0;
		}
		int val = this.jdbcTemplate.update(
				"update AXI_APP_DTL set currentStatus = ? , currentFormKey = ?, currentTaskId = ? , currentStep = ?   where id = ?",
				applicationTableRowInstance.getStatus(), applicationTableRowInstance.getCurrentFormKey(),
				applicationTableRowInstance.getCurrentTaskId(), applicationTableRowInstance.getCurrentStep(),applicationTableRowInstance.getId());
		sLogger.log(Level.INFO, "EXIT | update() status=" + val);
		return val;

	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public SimpleJdbcInsert getInsertApplication() {
		return insertApplication;
	}

	public void setInsertApplication(SimpleJdbcInsert insertApplication) {
		this.insertApplication = insertApplication;
	}

	public DataSource getAppDataSource() {
		return appDataSource;
	}

	public void setAppDataSource(DataSource appDataSource) {
		sLogger.log(Level.INFO, "ENTRY | setAppDataSource() " + appDataSource);
		this.jdbcTemplate = new JdbcTemplate(appDataSource);
		this.insertApplication = new SimpleJdbcInsert(appDataSource).withTableName(TABLE_NAME);
		ApplicationDatabaseService.instance().setAppTemplate(this);
		sLogger.log(Level.INFO, "EXIT | setAppDataSource() " + insertApplication.getTableName());
	}

	/**
	 * 
	 * @see http://docs.spring.io/spring/docs/current/spring-framework-reference
	 *      /html/jdbc.html
	 * @return
	 */

	public List<ApplicationTblRow> findAllApplications() {
		return this.jdbcTemplate.query(
				"select id, processDefinitionId , currentStatus , currentFormKey , currentTaskId , currentStep from AXI_APP_DTL",
				new ApplicationRowMapperTbl());
	}

	/**
	 * INNER CLASS - Maps the ResultSet Row into a Application Row Java Object 
	 * @author alok.mishra
	 *
	 */
	private static final class ApplicationRowMapperTbl implements RowMapper<ApplicationTblRow> {
		Logger sLogger = Logger.getLogger(ApplicationRowMapperTbl.class.getName());

		/**
		 * Map ResultSet rows by name to Application Row Java Object
		 * -AM
		 */
		public ApplicationTblRow mapRow(ResultSet rs, int rowNum) throws SQLException {
			sLogger.log(Level.INFO, "ENTRY | mapRow() ");
			ApplicationTblRow app = new ApplicationTblRow();
			app.setId(rs.getString("id"));
			app.setCamundaProcessId(rs.getString("processDefinitionId"));
			app.setCurrentApplicationStatus(rs.getString("currentStatus"));
			app.setCurrentFormKey(rs.getString("currentFormKey"));
			app.setCurrentTaskId(rs.getString("currentTaskId"));
			app.setCurrentStep(rs.getInt("currentStep"));
			sLogger.log(Level.INFO, "EXIT | mapRow() ");
			return app;
		}
	}
}
