package au.com.axicorp.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import au.com.axicorp.db.pojo.ApplicationFormVariableTblRow;
import au.com.axicorp.db.service.ApplicationDatabaseService;

@Repository
public class JDBCFormVariableTemplate implements FormVariableDAO {
	private Logger sLogger = Logger.getLogger(JDBCAppTemplate.class.getName());
	private static final String TABLE_NAME = "AXI_APP_FORM_VARS_DTL";

	/**
	 * 
	 */
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert formSimpleJDBCInsert;
	@Autowired
	private DataSource appDataSource;

	public JDBCFormVariableTemplate() {

	}

	/**
	 * INSERT INTO dbo.AXI_APP_FORM_VARS_DTL
	 */
	public void insert(ApplicationFormVariableTblRow appFormVariableRow) {
		sLogger.log(Level.INFO, "ENTRY | insert() " + this.getAppDataSource().toString());
		sLogger.log(Level.INFO, "        Params: " + appFormVariableRow.toString());
		Map<String, Object> parameters = new HashMap<String, Object>(5);
		parameters.put("id", appFormVariableRow.getId());
		parameters.put("formKey", appFormVariableRow.getFormKey());
		parameters.put("variableName", appFormVariableRow.getVariableName());
		parameters.put("variableType", appFormVariableRow.getVariableType());
		parameters.put("filterPattern", appFormVariableRow.getFilterPattern());
		this.formSimpleJDBCInsert.execute(parameters);
		sLogger.log(Level.INFO, "EXIT | insert() ");

	}

	/**
	 * GET ALL ROWS
	 */
	public List<ApplicationFormVariableTblRow> getAllRowsInFormVarsTable() {
		sLogger.log(Level.INFO, "ENTRY | getAllRowsInFormVarsTable() ");

		String sql = "select id, formKey , variableName , variableType , filterPattern from AXI_APP_FORM_VARS_DTL ";
		List<ApplicationFormVariableTblRow> formRows = this.jdbcTemplate.query(sql, new AppFormVariableRowMapperTbl());

		if (formRows != null)
			sLogger.log(Level.INFO, "        Result: Total Child Rows returned = " + formRows.size());
		else {
			formRows = new ArrayList<ApplicationFormVariableTblRow>();
			sLogger.log(Level.INFO, "        Result: Total Child Rows returned = 0 ");
		}
		sLogger.log(Level.INFO, "EXIT | getAllRowsInFormVarsTable() ");
		return formRows;

	}

	/**
	 * FIND ROW By FormKey
	 */
	public List<ApplicationFormVariableTblRow> getRowsInFormVarsTableByFormKey(String formKey) {
		sLogger.log(Level.INFO, "ENTRY | getAllRowsInFormVarsTable() ");

		String sql = "select id, formKey , variableName , variableType , filterPattern from AXI_APP_FORM_VARS_DTL where formKey = ? ";
		List<ApplicationFormVariableTblRow> formRows = this.jdbcTemplate.query(sql, new Object[]{formKey},new AppFormVariableRowMapperTbl());

		if (formRows != null)
			sLogger.log(Level.INFO, "        Result: Total Child Rows returned = " + formRows.size());
		else {
			formRows = new ArrayList<ApplicationFormVariableTblRow>();
			sLogger.log(Level.INFO, "        Result: Total Child Rows returned = 0 ");
		}
		sLogger.log(Level.INFO, "EXIT | getAllRowsInFormVarsTable() ");
		return formRows;
	}

	public void updateRowsInFormVarsTableById(String id, ApplicationFormVariableTblRow formVar) {
		sLogger.log(Level.INFO, "ENTRY | update() ");
		sLogger.log(Level.INFO, "        Params: " + formVar);
		this.jdbcTemplate.update(
				"update AXI_APP_FORM_VARS_DTL set  variableName = ? , variableType = ? , filterPattern = ? where  id= ?",
				formVar.getVariableName(), formVar.getVariableType(),formVar.getFilterPattern(), id);
		sLogger.log(Level.INFO, "EXIT | update() ");

	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public SimpleJdbcInsert getFormSimpleJDBCInsert() {
		return formSimpleJDBCInsert;
	}

	public void setFormSimpleJDBCInsert(SimpleJdbcInsert formSimpleJDBCInsert) {
		this.formSimpleJDBCInsert = formSimpleJDBCInsert;
	}

	public DataSource getAppDataSource() {
		return appDataSource;
	}

	/**
	 * setDatasource
	 * 
	 * @param appDataSource
	 */
	public void setAppDataSource(DataSource appDataSource) {
		sLogger.log(Level.INFO, "ENTRY | setAppDataSource() ");
		this.appDataSource = appDataSource;
		this.jdbcTemplate = new JdbcTemplate(appDataSource);
		this.formSimpleJDBCInsert = new SimpleJdbcInsert(appDataSource).withTableName(TABLE_NAME);
		ApplicationDatabaseService.instance().setFormvarTemplate(this);
		sLogger.log(Level.INFO, "EXIT | setAppDataSource() " + formSimpleJDBCInsert.getTableName());
	}

	/**
	 * INNER CLASS - Maps the ResultSet Row into a Form Table Java Object
	 * 
	 * @author alok.mishra
	 *
	 */
	private static final class AppFormVariableRowMapperTbl implements RowMapper<ApplicationFormVariableTblRow> {
		Logger sLogger = Logger.getLogger(AppFormVariableRowMapperTbl.class.getName());

		/**
		 * Map ResultSet rows by name to Form Variable -AM
		 */
		public ApplicationFormVariableTblRow mapRow(ResultSet rs, int rowNum) throws SQLException {
			sLogger.log(Level.INFO, "ENTRY | mapRow() ");
			ApplicationFormVariableTblRow app = new ApplicationFormVariableTblRow();
			app.setId(rs.getString("id"));
			app.setFormKey(rs.getString("formKey"));
			app.setVariableName(rs.getString("variableName"));
			app.setVariableType(rs.getString("variableType"));
			app.setFilterPattern(rs.getString("filterPattern"));
			sLogger.log(Level.INFO, "EXIT | mapRow() ");
			return app;
		}
	}

}
