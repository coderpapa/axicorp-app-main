package au.com.axicorp.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import au.com.axicorp.db.pojo.ApplicationFormTblRow;
import au.com.axicorp.db.pojo.ApplicationTblRow;
import au.com.axicorp.db.service.ApplicationDatabaseService;

@Repository
public class JDBCFormTemplate implements FormDAO {
	private Logger sLogger = Logger.getLogger(JDBCFormTemplate.class.getName());
	private static final String TABLE_NAME = "AXI_APP_FORM_DTL";
	
	/**
	 * 
	 */
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert formSimpleJDBCInsert;
	@Autowired
	private DataSource appDataSource;

	
	public JDBCFormTemplate(){
		super();
	}
	
	/**
	 * Insert Application Form 
	 * 
	 */
	public void insert(ApplicationFormTblRow applicationFormTblRow) {
		sLogger.log(Level.INFO, "ENTRY | insert() " + this.getAppDataSource().toString());
		sLogger.log(Level.INFO, "        Params: " + applicationFormTblRow.toString());
		Map<String, Object> parameters = new HashMap<String, Object>(8);
		parameters.put("id", applicationFormTblRow.getId());
		parameters.put("applicationId", applicationFormTblRow.getApplicationId());
		parameters.put("taskId", applicationFormTblRow.getTaskId());
		parameters.put("formKey", applicationFormTblRow.getFormKey());
		parameters.put("formFields", applicationFormTblRow.getFormFields());
		parameters.put("completed", applicationFormTblRow.getCompleted());
		parameters.put("createDateTime", new java.sql.Timestamp(System.currentTimeMillis()));
		parameters.put("updateDateTime", new java.sql.Timestamp(System.currentTimeMillis()));
		this.formSimpleJDBCInsert.execute(parameters);
		sLogger.log(Level.INFO, "EXIT | insert() ");

	}

	/**
	 * Query Application Forms By Application Id
	 */
	public List<ApplicationFormTblRow> findByApplicationId(String applicationId) {
		sLogger.log(Level.INFO, "ENTRY | findFormForAppIdUsingFormId() ");
		sLogger.log(Level.INFO, "        Params: {applicationId:" + applicationId + "}");
		
		
		String sql = "select id, applicationId , taskId , completed , formKey ,formFields, createDateTime , updateDateTime  from AXI_APP_FORM_DTL where applicationId = ?  ";
		List<ApplicationFormTblRow> formRows = this.jdbcTemplate.query(sql, new Object[] { applicationId },
				new AppFormRowMapperTbl());

		if (formRows != null)
			sLogger.log(Level.INFO, "        Result: Total Child Rows returned = " + formRows.size());
		else {
			formRows = new ArrayList<ApplicationFormTblRow>();
			sLogger.log(Level.INFO, "        Result: Total Child Rows returned = 0 ");
		}
		sLogger.log(Level.INFO, "EXIT | findFormForAppIdUsingFormId() ");
		return formRows;
	}

	/**
	 *  Query Application Forms by Application Id AND Form Key
	 */
	public ApplicationFormTblRow findByApplicationIdAndFormKey(String applicationId,String formKey) {
		sLogger.log(Level.INFO, "ENTRY | findFormForAppIdUsingFormId() ");
		sLogger.log(Level.INFO, "        Params: {applicationId:" + applicationId + "}");
		String sql = "select id, applicationId , taskId , completed , formKey ,formFields, createDateTime , updateDateTime  from AXI_APP_FORM_DTL where applicationId = ? and formKey ?  ";

		ApplicationFormTblRow formRow = this.jdbcTemplate.queryForObject(sql, new Object[] { applicationId, formKey },
				new AppFormRowMapperTbl());

		if (formRow != null)
			sLogger.log(Level.INFO, "        Result: ApplicationFormTblRow object " +formRow);
		else {
			formRow  = new ApplicationFormTblRow();
			sLogger.log(Level.INFO, "        Result: Total Child Rows returned = 0 ");
		}
		sLogger.log(Level.INFO, "EXIT | findFormForAppIdUsingFormId() ");
		return formRow;
	}

	/**
	 * Update Application Form by Application Id and new Form Row Object
	 */
	public void update(String applicationId, ApplicationFormTblRow applicationFormTblRow) {
		sLogger.log(Level.INFO, "ENTRY | update() ");
		sLogger.log(Level.INFO, "        Params: " + applicationFormTblRow);
		this.jdbcTemplate.update(
				"update AXI_APP_FORM_DTL set  completed = ?, formFields = ? , updateDateTime = ? where applicationId = ? and id= ?",
				applicationFormTblRow.getCompleted(), applicationFormTblRow.getFormFields(),
				new java.sql.Timestamp(System.currentTimeMillis()), applicationFormTblRow.getApplicationId(),
				applicationFormTblRow.getId());
		sLogger.log(Level.INFO, "EXIT | update() ");

	}

	
	/**
	 * getDataSource
	 * 
	 * @return
	 */
	public DataSource getAppDataSource() {
		return this.appDataSource;
	}

	/**
	 * setDatasource
	 * 
	 * @param appDataSource
	 */
	public void setAppDataSource(DataSource appDataSource) {
		sLogger.log(Level.INFO, "ENTRY | setAppDataSource() ");
		this.appDataSource = appDataSource;
		this.jdbcTemplate = new JdbcTemplate(appDataSource);
		this.formSimpleJDBCInsert = new SimpleJdbcInsert(appDataSource).withTableName(TABLE_NAME);
		ApplicationDatabaseService.instance().setFormTemplate(this);
		sLogger.log(Level.INFO, "EXIT | setAppDataSource() " + formSimpleJDBCInsert.getTableName());
	}

	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public SimpleJdbcInsert getInsertApplication() {
		return formSimpleJDBCInsert;
	}

	public void setInsertApplication(SimpleJdbcInsert insertApplication) {
		this.formSimpleJDBCInsert = insertApplication;
	}

	/**
	 * INNER CLASS - Maps the ResultSet Row into a Form Table Java Object 
	 * @author alok.mishra
	 *
	 */
	private static final class AppFormRowMapperTbl implements RowMapper<ApplicationFormTblRow> {
		Logger sLogger = Logger.getLogger(AppFormRowMapperTbl.class.getName());
		
		/**
		 * Map ResultSet rows by name to Form Row
		 * -AM
		 */
		public ApplicationFormTblRow mapRow(ResultSet rs, int rowNum) throws SQLException {
			sLogger.log(Level.INFO, "ENTRY | mapRow() ");
			ApplicationFormTblRow app = new ApplicationFormTblRow();
			app.setId(rs.getString("id"));
			app.setApplicationId(rs.getString("applicationId"));
			app.setTaskId(rs.getString("taskId"));
			app.setCompleted(rs.getString("completed"));
			app.setFormKey(rs.getString("formKey"));
			app.setFormFields(rs.getString("formFields"));
			app.setCreateTime(ApplicationFormTblRow.getTime(rs.getTimestamp("createDateTime")));
			app.setLastUpdateTime(ApplicationFormTblRow.getTime(rs.getTimestamp("updateDateTime")));
			sLogger.log(Level.INFO, "EXIT | mapRow() ");
			return app;
		}
	}

}
