package au.com.axicorp.db.pojo;

import au.com.axicorp.app.api.types.AxiApplication;

public class ApplicationTblRow {

	private String id;
	private String camundaProcessId;
	private String currentApplicationStatus;
	private String currentFormKey;
	private String currentTaskId;
	private int currentStep;

	public ApplicationTblRow() {
	}

	public ApplicationTblRow(AxiApplication application) {
		super();
		this.id = application.getApplicationId();
		this.camundaProcessId = application.getCamundaProcessId();
		this.currentFormKey = application.getCurrentFormKey();
		this.currentStep = application.getCurrentStep();
		this.currentTaskId = application.getCurrentTaskNumber();
		this.currentApplicationStatus = application.getStatus();
		 
	}

	public void updateInstance(AxiApplication newApp) {

		if (newApp.getCamundaProcessId() != null)
			this.camundaProcessId = newApp.getCamundaProcessId();
		if (newApp.getCurrentFormKey() != null)
			this.currentFormKey = newApp.getCurrentFormKey();
		if (newApp.getCurrentStep() > this.currentStep)
			this.currentStep = newApp.getCurrentStep();
		if (newApp.getCurrentTaskNumber() != null)
			this.currentTaskId = newApp.getCurrentTaskNumber();
		if (newApp.getStatus() != null)
			this.currentApplicationStatus = newApp.getStatus();
		
	}

	public AxiApplication toAxiApplication() {
		AxiApplication app = new AxiApplication();
		app.setApplicationId(this.id);
		app.setCamundaProcessId(this.camundaProcessId);
		app.setCurrentFormKey(this.currentFormKey);
		app.setCurrentStep(this.currentStep);
		app.setCurrentTaskNumber(this.currentTaskId);
		app.setStatus(this.getCurrentApplicationStatus());		
		// What about the sub-forms?
		app.getFormByStepMap();
		return app;
	}

	public String getCamundaProcessId() {
		return camundaProcessId;
	}

	public void setCamundaProcessId(String camundaProcessId) {
		this.camundaProcessId = camundaProcessId;
	}

	public String getStatus() {
		return currentApplicationStatus;
	}

	public void setStatus(String status) {
		this.currentApplicationStatus = status;
	}

	public String getCurrentFormKey() {
		return currentFormKey;
	}

	public void setCurrentFormKey(String currentFormKey) {
		this.currentFormKey = currentFormKey;
	}

	public String getCurrentTaskId() {
		return currentTaskId;
	}

	public void setCurrentTaskId(String currentTaskId) {
		this.currentTaskId = currentTaskId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCurrentApplicationStatus() {
		return currentApplicationStatus;
	}

	public void setCurrentApplicationStatus(String currentApplicationStatus) {
		this.currentApplicationStatus = currentApplicationStatus;
	}

	public int getCurrentStep() {
		return currentStep;
	}

	public void setCurrentStep(int currentStep) {
		this.currentStep = currentStep;
	}

	 
	@Override
	public String toString() {
		return String.format("Application[id= " + id + ", camundaProcessId= " + camundaProcessId
				+ ", currentApplicationStatus=" + currentApplicationStatus + ", currentFormKey=" + currentFormKey
				+ ", currentTaskId= " + currentTaskId);
	}

}
