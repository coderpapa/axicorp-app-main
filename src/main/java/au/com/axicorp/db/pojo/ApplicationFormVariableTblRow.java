package au.com.axicorp.db.pojo;

public class ApplicationFormVariableTblRow {
	private String id;
	private String formKey;
	private String variableName;
	private String variableType;
	private String filterPattern;

	public ApplicationFormVariableTblRow() {
		super();
	}

	public ApplicationFormVariableTblRow(String formKey, String varName, String varType, String filterPattern) {
		super();
		this.id=java.util.UUID.fromString(formKey+"-"+varName).toString();
		this.formKey=formKey;
		this.variableName=varName;
		this.variableType=varType;
		this.filterPattern=filterPattern;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFormKey() {
		return formKey;
	}

	public void setFormKey(String formKey) {
		this.formKey = formKey;
	}

	public String getVariableName() {
		return variableName;
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}

	public String getVariableType() {
		return variableType;
	}

	public void setVariableType(String variableType) {
		this.variableType = variableType;
	}

	public String getFilterPattern() {
		return filterPattern;
	}

	public void setFilterPattern(String filterPattern) {
		this.filterPattern = filterPattern;
	}

}
