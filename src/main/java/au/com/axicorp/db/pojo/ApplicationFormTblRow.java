package au.com.axicorp.db.pojo;

import java.sql.Timestamp;

import au.com.axicorp.app.api.types.AxiApplication;
import au.com.axicorp.app.api.types.AxiApplicationTask;
import au.com.axicorp.utils.Utils;

public class ApplicationFormTblRow {
	private String id;
	private String applicationId;
	private String taskId;
	private String formKey;
	private String formFields;
	private long createTime;
	private long lastUpdateTime;
	private boolean isCompleted;
	
	public ApplicationFormTblRow() {
		super();
	}
	
	public ApplicationFormTblRow(String applicationId,AxiApplicationTask f) {
		super();
		this.id=f.getTaskNumber();
		this.applicationId=applicationId;
		this.taskId=f.getTaskId();
		this.formKey=f.getTaskFormKey();
		this.formFields=f.getFormFieldsAsJSONString();
		this.createTime=f.getStart();
		this.lastUpdateTime=f.setFinish();
	}
	
	public AxiApplicationTask toForm(){
		AxiApplicationTask newForm = new AxiApplicationTask(id,formKey,taskId,formFields);
		newForm.setCompleted(isCompleted);
		return newForm;
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
 
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	
	
	public String getFormKey() {
		return formKey;
	}
	public void setFormKey(String formKey) {
		this.formKey = formKey;
	}
	
	
	public String getFormFields() {
		return formFields;
	}

	public void setFormFields(String formFields) {
		this.formFields = formFields;
	}

	public long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}
	public long getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(long lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public boolean isCompleted() {
		return isCompleted;
	}
	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}
	public void setCompleted(String s) {
		this.isCompleted=false;
		if(!Utils.isEmpty(s))
		{
			if(s.toUpperCase().equals("Y"))
				this.isCompleted=true;
		}		
		
	}
	public static long getTime(Timestamp timestamp) {
		if(timestamp != null)
		{
			return timestamp.getTime();			
		}		
		return -1;
	}
	public String getCompleted() {
		return isCompleted?"Y":"N";
	}

 
	 
	 
	
	
}
