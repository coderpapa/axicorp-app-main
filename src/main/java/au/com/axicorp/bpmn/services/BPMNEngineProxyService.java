package au.com.axicorp.bpmn.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngineServices;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.repository.ProcessDefinitionQuery;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.model.bpmn.instance.Definitions;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.axicorp.utils.Constants;
import au.com.axicorp.utils.ConfigPropertyService;
import au.com.axicorp.utils.Utils;

public class BPMNEngineProxyService {
	private static BPMNEngineProxyService instance = null;
	private static final String processDefinitionKey = ConfigPropertyService.instance().getProperties()
			.getProperty(Constants.PROCESS_DEF_KEY);
	Logger sLogger = Logger.getLogger(BPMNEngineProxyService.class.getName());

	@Autowired
	ProcessEngine processEngine;

	private Map<String,String[]> formVarsByFormKey = new HashMap<String,String[]>();
	
	public static BPMNEngineProxyService instance() {
		if (instance == null) {
			instance = new BPMNEngineProxyService();
		}
		return instance;
	}

	/**
	 * Start A BPMN Process with Process Vars
	 * 
	 * @param requestMap
	 * @return
	 */
	public ProcessInstance startProcess(Map<String, String> requestMap) {
		ProcessInstance instance = getEngine().getRuntimeService().startProcessInstanceByKey(processDefinitionKey,
				Utils.toObjectMap(requestMap));
		return instance;
	}
	
	

	/**
	 * Update Task Var
	 * 
	 */
	public void updateTask(String taskId, Map<String, Object> variables) {
		//getEngine().getTaskService().setVariables(taskId, variables);
		getEngine().getTaskService().setVariablesLocal(taskId, variables);
	}

	/**
	 * Update Task Var & Complete Task
	 * 
	 */
	public void completeTask(String taskId, Map<String, Object> variables) {
		updateTask(taskId, variables);
		getEngine().getTaskService().complete(taskId);
	}
	

	/**
	 * Get Task Var by TaskId
	 * 
	 * @param taskId
	 * @return
	 */
	public Map<String, Object> getTaskVariables(String taskId) {
		if (!Utils.isEmpty(taskId)) {
			Map<String, Object> variable = getEngine().getTaskService().getVariablesLocal(taskId);
			return variable;
		} else {
			return Constants.EMPTY_TASK_VARS;
		}
	}

	
	
	/**
	 * Query task id by the process id
	 * 
	 * AM: 26/06/2015 !TODO Filter by Non-Admin User Later
	 * 
	 * @param camundaProcessId
	 * @return
	 */
	public String getCurrentTaskByProcessId(String camundaProcessId) {
		sLogger.log(Level.INFO, "[getCurrentTaskByProcessId]     			ENTRY | Get current taskid for processid: "
				+ camundaProcessId);
		if (Utils.isEmpty(camundaProcessId))
			return "";

		Task task = getEngine().getTaskService().createTaskQuery().initializeFormKeys()
				.processInstanceId(camundaProcessId).active().singleResult();
		if (task == null) {
			sLogger.log(Level.INFO, "[getCurrentTaskByProcessId]     			EXIT | No Task Found for processid: "
					+ camundaProcessId);
			return "";
		} else {
			sLogger.log(Level.INFO, "[getCurrentTaskByProcessId]     			EXIT | Task for processid: "
					+ camundaProcessId + " is taskid:" + task.getId());
			return task.getId();
		}
	}

	public Task getTaskByProcessInstanceId(String processInstanceId) {
		Task task = getEngine().getTaskService().createTaskQuery().initializeFormKeys()
				.processInstanceId(processInstanceId).active().singleResult();
		if (task == null) {
			sLogger.log(Level.INFO, "No Task Matching PID was found ");
		} else {
			sLogger.log(Level.INFO, "Task Matching processInstanceId :" + processInstanceId + " is " + task.getId());
		}
		return task;
	}

	/**
	 * Get Camunda Process Engine Instance
	 * 
	 * @return
	 */
	public ProcessEngineServices getEngine() {
		if (processEngine == null){
			processEngine= ProcessEngines.getDefaultProcessEngine();
			if(processEngine ==null)
			{
				processEngine= ProcessEngines.getProcessEngine("engine");
			}
		}
		return processEngine;
	}

	/**
	 * Set Camunda Process Engine Instance
	 * 
	 * @param processEngine
	 */
	public void setProcessEngine(ProcessEngine processEngine) {
		sLogger.log(Level.INFO, "Process Engine was set using @Autowired ");
		this.processEngine = processEngine;
	}

	/**
	 * Embedded Form Key as defined in the BPMN flow for the given Task
	 * 
	 * @param processInstanceId
	 * @return
	 */
	public String getFormKeyByTaskId(String taskId) {
		Task task = getTaskById(taskId);
		if (task != null)
			return getEngine().getFormService().getTaskFormKey(task.getProcessDefinitionId(),
					task.getTaskDefinitionKey());
		else {
			sLogger.log(Level.INFO, "Could not find form key for taskId: " + taskId + " task was null ");
			return "";
		}
	}
	
	/**
	 * 
	 * @param taskId
	 * @return
	 */
	public String[] getFormVarsByTaskId(String taskId) {	
		Task task = getTaskById(taskId);
		String formKey = getFormKeyByTaskId(taskId);
		String[] formVars = formVarsByFormKey.get(formKey);
		if(formVars == null)
		{
			if (task != null){
				VariableMap map=  getEngine().getFormService().getTaskFormVariables(taskId);
				formVars = map.keySet().toArray(new String[]{}); 
			}		
			else {
				sLogger.log(Level.INFO, "Could not find form vars for taskId: " + taskId + " task was null ");
				formVars= new String[]{};
			}
			
			formVarsByFormKey.put(formKey, formVars);
		}
		return formVars;		
	}
	
	public Map<String,String> getDefaultVarMapByTaskId(String taskId){
		String[] formVars = getFormVarsByTaskId(taskId);
		if(formVars == null || formVars.length ==0)
			return new HashMap<String,String>();
		Map<String,String> defaultMap = new HashMap<String,String>(formVars.length);
		for(int i=0;i<formVars.length;i++)
			defaultMap.put(formVars[i], "");
		return defaultMap;
	}

	/**
	 * Query Task By Id
	 * 
	 * @param taskId
	 * @return
	 */
	public Task getTaskById(String taskId) {
		Task task = getEngine().getTaskService().createTaskQuery().initializeFormKeys().taskId(taskId).singleResult();
		if (task == null) {
			sLogger.log(Level.INFO, "Could not find Task by taskId: " + taskId + " task was null ");
		}
		return task;
	}


}
