package au.com.axicorp.bpmn.services;

import org.camunda.bpm.engine.ProcessEngineServices;
import org.camunda.bpm.engine.ProcessEngines;

public class BPMNEngineService {
	private static BPMNEngineService instance = null;
	private ProcessEngineServices processEngine;
	public static BPMNEngineService instance(){
		if(instance == null){
			instance = new BPMNEngineService();
		}
		return instance;
	}
	
	public static ProcessEngineServices getEngine(){
		return BPMNEngineService.instance().getProcessEngine();
	}
	

	/**
	 * Get Camunda Process Engine Instance
	 * 
	 * @return
	 */
	public ProcessEngineServices getProcessEngine() {		 
		if (processEngine == null){
			processEngine= ProcessEngines.getDefaultProcessEngine();
			if(processEngine ==null)
			{
				processEngine= ProcessEngines.getProcessEngine("engine");
			}
		}
		return processEngine;
	}
}
