package au.com.axicorp.bpmn.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.camunda.bpm.engine.ProcessEngineServices;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.ExtensionElements;
import org.camunda.bpm.model.bpmn.instance.UserTask;

import au.com.axicorp.app.api.types.ValidFormFields;
import au.com.axicorp.utils.ConfigPropertyService;
import au.com.axicorp.utils.Constants;
import au.com.axicorp.utils.Utils;

import org.camunda.bpm.model.bpmn.instance.camunda.CamundaFormData;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaFormField;
import org.json.JSONObject;

public class BPMNModelService {
	private static Logger sLogger = Logger.getLogger(BPMNModelService.class.getName());
	private static BPMNModelService instance = null;
	protected BpmnModelInstance modelInstance;

	Map<String, ValidFormFields> formKeyByFormKeyFieldsMap = new HashMap<String, ValidFormFields>();

	protected BPMNModelService() {
		super();
	}

	public static BPMNModelService instance() {
		if (instance == null) {
			instance = new BPMNModelService().load();
		}
		return instance;
	}
	
	public static void main(String[] args) {
		ValidFormFields fields = new ValidFormFields();
		for (int i = 0; i < 10; i++) 
			fields.add(i + " type", i + "", true, i + " decsr");
		 
		sLogger.log(Level.INFO, "[getFormFields()]             fields:	" + fields);
	}

	protected BPMNModelService load() {
		sLogger.log(Level.INFO, "[load()] ENTRY	");
		Collection<UserTask> c = getUserTasks();
		for (UserTask t : c) {
			String formKey = t.getCamundaFormKey();
			ValidFormFields v = getFormFields(t);
			sLogger.log(Level.INFO,
					"[load()] 			adding to formKeyByFormKeyFieldsMap:	<Key:" + formKey + ",Value:" + v.toString() + ">");

			formKeyByFormKeyFieldsMap.put(formKey, v);
		}
		sLogger.log(Level.INFO, "[load()] Exit	");

		return this;
	}

	/**
	 * Get Form Fields By Form Keys
	 * 
	 * @param formKey
	 * @return
	 */
	public ValidFormFields getFormFieldsByFormKey(String formKey) {
		ValidFormFields fields = formKeyByFormKeyFieldsMap.get(formKey);
		sLogger.log(Level.INFO,
				"[getFormFieldsByFormKey()] 			Returning from formKeyByFormKeyFieldsMap:	<Key:" + formKey + ",Value:" + fields.toString() + ">");
		if (fields == null)
			fields = new ValidFormFields();

		return fields;
	}

	/**
	 * Get Valid Form Keys
	 * 
	 * @return
	 */
	public Set<String> getFormKeys() {		 
			sLogger.log(Level.INFO, "[getFormKeys()] ENTRY	");

		return formKeyByFormKeyFieldsMap.keySet();
	}

	/**
	 * See if the fields are valid
	 * 
	 * @param formKey
	 * @param values
	 * @return
	 */
	public boolean hasRequiredFields(String formKey, Map<String, String> values) {
		ValidFormFields fields = getFormFieldsByFormKey(formKey);
		// No Restrictions
		if (fields == null)
			return true;
		// Some Restrictions
		return fields.hasRequiredFields(values);
	}

	/**
	 * For the Camunda Task, it looks for the FormData elements and returns a
	 * list of their ids
	 * 
	 * @param t
	 * @return
	 */
	private ValidFormFields getFormFields(UserTask t) {
		sLogger.log(Level.INFO, "[getFormFields()] ENTRY	");
		ValidFormFields fields = new ValidFormFields();
		ExtensionElements extensionElements = t.getExtensionElements();
		CamundaFormData formData = extensionElements.getElementsQuery().filterByType(CamundaFormData.class)
				.singleResult();
		sLogger.log(Level.INFO, "[getFormFields()]              Got form data:	");
		for (CamundaFormField formField : formData.getCamundaFormFields()) {
			if (formField != null) {
				fields.add(formField.getCamundaId(), formField.getCamundaType(), true, formField.getCamundaLabel());
			}
			sLogger.log(Level.INFO, "[getFormFields()]             fields:	" + fields);
		}
		sLogger.log(Level.INFO, "[getFormFields()] EXIT	");
		return fields;
	}
	 


	private Collection<UserTask> getUserTasks() {
		Collection<UserTask> c = getModelInstance().getModelElementsByType(UserTask.class);
		return c;
	}

	private BpmnModelInstance getModelInstance() {
		if (modelInstance == null) {
			ProcessDefinition pd = BPMNProcessDeployerService.instance()
					.getProcessDefinitionByName(getProcessDefinitionKey());

			if (pd != null) {
				modelInstance = BPMNEngineService.getEngine().getRepositoryService().getBpmnModelInstance(pd.getId());
			} else {
				// TODO FATAL Error!
			}
		}
		return modelInstance;
	}

	private String getProcessDefinitionKey() {
		String processDefinitionKey = ConfigPropertyService.instance().getProperties()
				.getProperty(Constants.PROCESS_DEF_KEY);
		if (Utils.isEmpty(processDefinitionKey)) {
			return Constants.PROCES_DEF_DEFAULT_VALUE;
		}
		return processDefinitionKey;
	}

}
