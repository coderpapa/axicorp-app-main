package  au.com.axicorp.bpmn.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.repository.Deployment;
import org.camunda.bpm.engine.repository.ProcessDefinition;

import au.com.axicorp.utils.Constants;
import au.com.axicorp.utils.ConfigPropertyService;
import au.com.axicorp.utils.Utils;

/**
 * Manages Deployment of the
 * 
 * @author alok.mishra
 *
 */
public class BPMNProcessDeployerService {
	Logger sLogger = Logger.getLogger(BPMNProcessDeployerService.class.getName());

	public static BPMNProcessDeployerService instance() {
		return new BPMNProcessDeployerService();
	}
	
	/**
	 * Deploy The Default flow
	 */
	public void deployProcess() {
		String processDefinitionKey = ConfigPropertyService.instance().getProperties().getProperty(Constants.PROCESS_DEF_KEY);
		deployProcess(processDefinitionKey);
	}
	
	/**
	 * Deploy The flow using a processdefinition key 
	 * 
	 * Load the BPMN File /META-INF/<bpmn>.bpmn and deploy
	 * 
	 * @param processDefinitionKey
	 */
	public void deployProcess(String processDefinitionKey){
 		sLogger.log(Level.INFO, "[deployProcess()] Entry | ~~~~~~~~~~~~~~~~~~ Starting: BPMN Deploy   ~~~~~~~~~~~~~~~~~~");
		String bpmnFile = ConfigPropertyService.instance().getProperties().getProperty(Constants.DEFAULT_BPMN_FILE_KEY);
		sLogger.log(Level.INFO, "[deployProcess()]                          {ProcessDefinitionKey: "+processDefinitionKey);
		if (Utils.isEmpty(bpmnFile)) {
			bpmnFile = Constants.DEFAULT_BPMN_FILE_DEFAULT_VALUE;
		}
		sLogger.log(Level.INFO, "[deployProcess()]                          {bpmnFile: "+bpmnFile);	
		Deployment deployment = BPMNEngineService.getEngine().getRepositoryService().createDeployment().name(processDefinitionKey).addClasspathResource(bpmnFile).deploy();
		log(deployment);
		logProcessDefinitions();		
		sLogger.log(Level.INFO, "[deployProcess()] Exit |  ~~~~~~~~~~~~~~~~~~ Completed:  BPMN Deploy  ~~~~~~~~~~~~~~~~~~");
	}






	public List<String> getDeployedProcesses() {
 		List<Deployment> deployments = BPMNEngineService.getEngine().getRepositoryService().createDeploymentQuery().list();
		List<String> deploymentsString = new ArrayList<String>();
		for (Deployment deployment : deployments) {
			deploymentsString.add("id:" + deployment.getId() + ",Name:" + deployment.getName() + ",Class:"
					+ deployment.getClass().getName() + ", Time:" + deployment.getDeploymentTime());
		}
		return deploymentsString;
	}

	
 
	public List<ProcessDefinition> getProcessDefinitions(){
		sLogger.log(Level.INFO, "[getProcessDefinitions] ");
 		List<ProcessDefinition> processDefinitions =BPMNEngineService.getEngine().getRepositoryService().createProcessDefinitionQuery().list();
		sLogger.log(Level.INFO, "[getProcessDefinitions] ");
		return processDefinitions;
	}

	public ProcessDefinition getProcessDefinitionByName(String processDefinitionKey ){
		sLogger.log(Level.INFO, "[getProcessDefinitionByName] ");
 		ProcessDefinition active = null;
 		 
 		
		List<ProcessDefinition> processes =BPMNEngineService.getEngine().getRepositoryService().createProcessDefinitionQuery().processDefinitionKey(processDefinitionKey).list();
		sLogger.log(Level.INFO, "[getProcessDefinitionByName()] ProcessDefinition | Got a list of  "+processes.size()+" for Key:"+processDefinitionKey);
		for(ProcessDefinition process:processes)
		{
			active = process; 
			sLogger.log(Level.FINEST, "[getProcessDefinitionByName()] ProcessDefinition |  id: "+process.getId()+", key:"+process.getKey()+", name:"+process.getName());
		}
		sLogger.log(Level.INFO, "[getProcessDefinitionByName] ");
		return active;
	}
	
	
	
	private void logProcessDefinitions() {
		List<ProcessDefinition> pds= getProcessDefinitions();
		for(ProcessDefinition pd:pds){
			sLogger.log(Level.INFO, "                         {Process Definition Id: "+pd.getId());
			sLogger.log(Level.INFO, "                         {Process Definition Key: "+pd.getKey());
			sLogger.log(Level.INFO, "                         {Process Definition Name: "+pd.getName());
			sLogger.log(Level.INFO, "                         {Process Definition Resource: "+pd.getResourceName());
		}
		
	}
	
	

	private void log(Deployment deployment) {
		sLogger.log(Level.INFO, "                         		### Deployment Completed ### ");	
		sLogger.log(Level.INFO, "                         {Deployment Id: "+deployment.getId());
		sLogger.log(Level.INFO, "                         {Deployment Name: "+deployment.getName());
		sLogger.log(Level.INFO, "                         {Deployment Date: "+deployment.getDeploymentTime());
		
	}
}
