package au.com.axicorp.bpmn.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.axicorp.utils.Constants;
import au.com.axicorp.utils.ConfigPropertyService;

 

/**
 * If there are any InMemory Maps Like ApplicationId->ProcessId etc they could
 * be loaded now
 * 
 * @author alok.mishra
 *
 */
public class CamundaStartupProcessor implements InitializingBean {
	Logger sLogger = Logger.getLogger(CamundaStartupProcessor.class.getName());

	@Autowired
	ProcessEngine processEngine;

	/**
	 * 
	 * @throws Exception
	 */
	public void afterPropertiesSet() throws Exception {
		sLogger.log(Level.INFO, "[afterPropertiesSet] Entry |  Loading the deployment .... ");
		String processDefinitionKey = ConfigPropertyService.instance().getProperties().getProperty(Constants.PROCESS_DEF_KEY);

		ProcessDefinition pd = BPMNProcessDeployerService.instance().getProcessDefinitionByName(processDefinitionKey);
		if (pd == null)
		{
			sLogger.log(Level.INFO, "[afterPropertiesSet]  #########  Deploying Process Flow Using Id "+processDefinitionKey);
			BPMNProcessDeployerService.instance().deployProcess(processDefinitionKey);
		}
		
		BPMNModelService modelService = BPMNModelService.instance();
		sLogger.log(Level.INFO, "[afterPropertiesSet] Exit  ");

	}

	/**
	 * 
	 * 
	 * @return
	 */
	public ProcessEngine getProcessEngine() {
		if (processEngine == null) {
			sLogger.log(Level.INFO,
					"Process Engine was null - creating using ProcessEngines.getDefaultProcessEngine(); ");
			processEngine = ProcessEngines.getDefaultProcessEngine();
		} else {
			sLogger.log(Level.INFO, "Using pre-defined Process Engine  ");

		}
		return processEngine;
	}

	public void setProcessEngine(ProcessEngine processEngine) {
		sLogger.log(Level.INFO, "Process Engine was set using @Autowired ");
		this.processEngine = processEngine;
	}

}
