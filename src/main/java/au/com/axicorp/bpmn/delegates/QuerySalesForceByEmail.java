package au.com.axicorp.bpmn.delegates;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.impl.util.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import au.com.axicorp.app.apiclient.LeadAPIAdapter;
import au.com.axicorp.app.types.SalesforceContactQueryRequest;
import au.com.axicorp.app.types.SalesforceContactQueryResponse;
import au.com.axicorp.bpmn.services.CamundaStartupProcessor;
import au.com.axicorp.utils.ConfigPropertyService;
import au.com.axicorp.utils.Constants;
import au.com.axicorp.utils.Utils;

/**
 * QuerySalesForceByEmail uses Process and Task-P1 vars
 * 
 * @author alok.mishra
 *
 */
public class QuerySalesForceByEmail implements JavaDelegate {
	Logger sLogger = Logger.getLogger(QuerySalesForceByEmail.class.getName());
	
	
	/**
	 * 
	 */
	public void execute(DelegateExecution execution) throws Exception {
		sLogger.log(Level.INFO, "    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ");
		sLogger.log(Level.INFO, "    				Begin Java Delegate.  Delegate Name = QuerySalesForceByEmail ");

		/**
		 * Process Level Vars
		 */
		String applicationId = (String) execution.getVariable(Constants.ApplicationId);
		String firstName = (String) execution.getVariable( "firstName");
		String lastName = (String) execution.getVariable("lastName");
		String email = (String) execution.getVariable("email");
		String phone = (String) execution.getVariable("phone");

		
		sLogger.log(Level.INFO, "                           Data: ");
		sLogger.log(Level.INFO, "                         	     Application Id: " + applicationId);
		sLogger.log(Level.INFO, "                          	     First Name: " + firstName);
		sLogger.log(Level.INFO, "                            	 Last Name: " + lastName);
		sLogger.log(Level.INFO, "                                Email: " + email);
		sLogger.log(Level.INFO, "                                Phone: " + phone);

		SalesforceContactQueryRequest salesForceRequest = new SalesforceContactQueryRequest();
		salesForceRequest.setEmail(email);
		salesForceRequest.setFirstName(firstName);
		salesForceRequest.setLastName(lastName);
		salesForceRequest.setPhone(phone);
		
		SalesforceContactQueryResponse salesForceResponse = new LeadAPIAdapter().search(salesForceRequest);		 
		execution.setVariable("isUserInSalesForce", salesForceResponse.isUserInSalesForce());
		execution.setVariable("isExistingContact", salesForceResponse.isExistingContact());
		execution.setVariable("isExistingLead", salesForceResponse.isExistingLead());
		execution.setVariable("isIndividualAcct", salesForceResponse.isIndividualAcct());
		execution.setVariable("isJointAcct", salesForceResponse.isJointAcct());
		execution.setVariable("isCompanyAcct", salesForceResponse.isCompanyAccount());
		
		sLogger.log(Level.INFO, "    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ");

	}
 

 
 
	 

}
