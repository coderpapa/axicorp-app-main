package au.com.axicorp.form.constants;

import java.util.HashMap;
import java.util.Map;

import au.com.axicorp.app.api.types.ValidFormFields;

public class StaticFormVariables {
	private static String[][] formP1_BasicContactDetailsForm = {{"firstName","String"},{"lastName","String"},{"email","String"},{"phone","String"},{"countryOfResidence","String"},{"accountType","String"}};
	private static String[][] formP1_Hidden= {{"promoCode","String"},{"formType","String"}};

	private static String[][] formP3_PrimaryApplicantDetailsForm_ApplicantDetails = {{"title","String"},{"firstName","String"},{"middleName","String"},{"surname","String"},{"dateOfBirth","String"},{"driversLicenseNumber","String"},{"country","String"}};
	private static String[][] formP3_PrimaryApplicantDetailsForm_ContactDetails = {{"email","String"},{"phone","String"}};
	private static String[][] formP3_PrimaryApplicantDetailsForm_ResidentialAddress = {{"propertyName","String"},{"unitNumber","String"},{"streetNumber","String"},{"streetName","String"},{"streetType","String"},{"suburb","String"},{"state","String"},{"postcode","String"}};
	private static String[][] formP3_Hidden= {};

	private static String[][] formP2_PlatformSetupForm = {{"language","String"},{"productSelection","String"},{"currency","String"},{"leverage","String"},{"referredBy","String"}};
	private static String[][] formP2_Hidden={};
	
	private static String[][] formP15_AdditionalUserInfoForm = {{"openAnotherTradingAccount","String"},{"differentClientAgreementType","String"}};
	private static String[][] formP15_Hidden={};
	
	
	private static String[][] formP6_NominatedBankAccountForm = {{"country","String"},{"bankOrInstitutionName","String"},{"branch","String"},{"accountName","String"},{"bankIdentifierCode","String"},{"bankAccountNumber","String"}};    
	private static String[][] formP6_Hidden={};
	

	private static String[][] formP7_AuthorizedRepDetailsForm_AuthorisedRepDetails = {{"title","String"},{"firstName","String"},{"middleName","String"},{"surname","String"},{"dateOfBirth","String"},{"driversLicenseNumber","String"},{"country","String"}};
	private static String[][] formP7_AuthorizedRepDetailsForm_ContactDetails = {{"email","String"},{"phone","String"}};
	private static String[][] formP7_AuthorizedRepDetailsForm_ResidentialAddress = {{"propertyName","String"},{"unitNumber","String"},{"streetNumber","String"},{"streetName","String"},{"streetType","String"},{"suburb","String"},{"state","String"},{"postcode","String"}};
	private static String[][] formP7_AuthorizedRepDetailsForm_AdditionalInformation = {{"relationshipToAcctHolder","String"},{"financialServicesLicenceNumber","String"}};
	private static String[][] formP7_Hidden= {};

	private static Map<String,ValidFormFields> formKeyToFormVarMapping = new HashMap<String,ValidFormFields>();
	
	static{ 
//		formKeyToFormVarMapping.put("BasicContactDetailsForm",  arrToMap(formP1_BasicContactDetailsForm).add(formP1_Hidden));
//		formKeyToFormVarMapping.put("PrimaryApplicantDetailsForm",  arrToMap(formP3_PrimaryApplicantDetailsForm_ApplicantDetails).add(formP3_PrimaryApplicantDetailsForm_ContactDetails).add(formP3_PrimaryApplicantDetailsForm_ResidentialAddress));
//		formKeyToFormVarMapping.put("PlatformSetupForm",  arrToMap(formP2_PlatformSetupForm));		
//		formKeyToFormVarMapping.put("AdditionalUserInfoForm",  arrToMap(formP15_AdditionalUserInfoForm));		
//		formKeyToFormVarMapping.put("NominatedBankAccountForm",  arrToMap(formP6_NominatedBankAccountForm));
//		formKeyToFormVarMapping.put("AuthorizedRepDetailsForm",  arrToMap(formP7_AuthorizedRepDetailsForm_AuthorisedRepDetails).add(formP7_AuthorizedRepDetailsForm_ContactDetails).add(formP7_AuthorizedRepDetailsForm_ResidentialAddress).add(formP7_AuthorizedRepDetailsForm_AdditionalInformation));
	}

	
	private ValidFormFields basicContactDetailsForm;
	public ValidFormFields getBasicContactDetailsForm() {
		return basicContactDetailsForm;
	}

	public void setBasicContactDetailsForm(ValidFormFields basicContactDetailsForm) {
		this.basicContactDetailsForm = basicContactDetailsForm;
	}

	private ValidFormFields primaryApplicantDetailsForm;
	private ValidFormFields platformSetupForm;
	private ValidFormFields additionalUserInfoForm;
	private ValidFormFields nominatedBankAccountForm;
	private ValidFormFields authorizedRepDetailsForm;
	
  
 
	public ValidFormFields getPrimaryApplicantDetailsForm() {
		return primaryApplicantDetailsForm;
	}

	public void setPrimaryApplicantDetailsForm(ValidFormFields primaryApplicantDetailsForm) {
		this.primaryApplicantDetailsForm = primaryApplicantDetailsForm;
	}

	public ValidFormFields getPlatformSetupForm() {
		return platformSetupForm;
	}

	public void setPlatformSetupForm(ValidFormFields platformSetupForm) {
		this.platformSetupForm = platformSetupForm;
	}

	public ValidFormFields getAdditionalUserInfoForm() {
		return additionalUserInfoForm;
	}

	public void setAdditionalUserInfoForm(ValidFormFields additionalUserInfoForm) {
		this.additionalUserInfoForm = additionalUserInfoForm;
	}

	public ValidFormFields getNominatedBankAccountForm() {
		return nominatedBankAccountForm;
	}

	public void setNominatedBankAccountForm(ValidFormFields nominatedBankAccountForm) {
		this.nominatedBankAccountForm = nominatedBankAccountForm;
	}

	public ValidFormFields getAuthorizedRepDetailsForm() {
		return authorizedRepDetailsForm;
	}

	public void setAuthorizedRepDetailsForm(ValidFormFields authorizedRepDetailsForm) {
		this.authorizedRepDetailsForm = authorizedRepDetailsForm;
	}

 
	
	
}
