USE [Camunda]
GO

/****** Object:  Table [dbo].[AXI_APP_FORM_DTL]    Script Date: 9/07/2015 2:56:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AXI_APP_FORM_DTL](
	[Id] [nvarchar](64) NULL,
	[applicationId] [nvarchar](64) NULL,
	[taskId] [nvarchar](64) NULL,
	[completed] [nvarchar](1) NULL,
	[formKey] [nvarchar](1000) NULL,
	[createDateTime] [datetime] NULL,
	[updateDateTime] [datetime] NULL,
	[formFields] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

