USE [Camunda]
GO

/****** Object:  Table [dbo].[AXI_APP_DTL]    Script Date: 9/07/2015 2:51:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AXI_APP_DTL](
	[id] [nvarchar](64) NOT NULL,
	[processDefinitionId] [nvarchar](64) NULL,
	[currentStatus] [nvarchar](50) NULL,
	[currentFormKey] [nvarchar](1000) NULL,
	[currentTaskId] [nvarchar](64) NULL,
	[currentStep] [int] NULL,
	[appFormMap] [nvarchar](max) NULL,
	[createDateTime] [datetime] NULL,
	[updateDateTime] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

