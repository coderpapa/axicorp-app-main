@echo off
REM -----------------------------------------------------------------------------------
REM  Applications API Build Script
REM   version: 1.0
REM   Created: 24/06/2015
REM   Author : Alok Mishra 
REM   Description:
REM     Calls Maven Clean and Install and then copies the WAR to Tomcat Webapps
REM -----------------------------------------------------------------------------------
call cls
@echo ON
@echo  
@echo              [buildrelease.bat]     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@echo              [buildrelease.bat]    	 Starting: Build, Release, Test 
@echo              [buildrelease.bat]     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@echo  
@echo  
@echo              [buildrelease.bat]     ========================
@echo              [buildrelease.bat]     1. Cleaning ....
@echo              [buildrelease.bat]     ========================
call mvn clean
@echo  
@echo  
@echo              [buildrelease.bat]     ========================
@echo              [buildrelease.bat]     2. Building WAR ....
@echo              [buildrelease.bat]     ========================
call mvn install
@echo  
@echo  
@echo              [buildrelease.bat]     ========================
@echo              [buildrelease.bat]     3. Copying to Tomcat Webapps ....
@echo              [buildrelease.bat]     ========================
call copy C:\Users\Alok.Mishra\workspace\ApplicationAPI\target\ApplicationAPI-0.0.1.war C:\Users\Alok.Mishra\ApplyOnlineCode\Camunda\server\apache-tomcat-7.0.62\webapps
 